﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Windows.Input;
using Microsoft.Surface.Presentation.Controls;

namespace Microsoft.Surface.Samples.ScatterPuzzle
{
    public class Logger
    {
        /* Types */
        public struct PuzzlePiece
        {
            public string id;
            public double x;
            public double y;
            public double size;
            public double angle;

            public PuzzlePiece(ScatterViewItem item)
            {
                this.id = item.Uid;
                this.x = item.Center.X;
                this.y = item.Center.Y;
                this.size = item.Width * item.Height;
                this.angle = item.Orientation;
            }
        };

        public int comparisonIndex = 0;

        public enum ActivityType { JOIN, COMPLETED };
        public enum EventType { FINGER, PUZZLE, ACTIVITY };

        /* Constants */
        private const string ACTIVITY_LINE_FORMAT = "{0},{1},{2}"; // <timestamp>,<eventtype>,<activitytype>
        private const string FINGER_LINE_FORMAT = "{0},{1},{2},{3},{4},{5},{6}"; // <timestamp>,<eventtype>,<fingerid>,<itemid>,<fingeraction>,<x>,<y>
        private const string FILENAME_FORMAT = "{0}.csv";
        private const string PUZZLE_LINE_FORMAT = "{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11}"; // <timestamp>,<eventtype>,<fingerid>,<itemid>,<x>,<y>,<angle>,<size>,<xdelta>,<ydelta>,<angledelta>,<sizedelta>
        private const double THRESH = 0.001;

        /* Properties */
        public string FileName { get; private set; }
        public string Log { get; private set; }
        public Dictionary<string, PuzzlePiece> Pieces { get; private set; }

        private int gestureNo = 0;
        private static string xsamples = "Xsamples.txt";
        private static string ysamples = "Ysamples.txt";
        private static string imuxsamples = "imuXsamples.txt";
        private static string imuysamples = "imuYsamples.txt";

        private StreamWriter identificationLogger;

        /* Constructor */
        public Logger()
        {
            FileName = string.Format(FILENAME_FORMAT, GetUnixEpoch());
            Pieces = new Dictionary<string, PuzzlePiece>();
            identificationLogger = new StreamWriter("data.txt");
        }

        public void logIdentification(int touches, int timestamp, int length, double[] diff, int sensorID)
        {
            
            identificationLogger.WriteLine(touches + "," + timestamp + "," + length + "," + + sensorID);
        }

        public void NextGesture()
        {
            gestureNo++;
        }

        /* Public Member Functions */
        public void LogEvent(TouchPoint tp, ScatterViewItem item)
        {
            /*int fId = tp.TouchDevice.Id;
            double x = tp.Position.X;
            double y = tp.Position.Y;
            int action = (int)tp.Action;

            LogEvent(fId, x, y, action, item);*/
        }


        public void LogEvent(int fId, double x, double y, int action, ScatterViewItem item)
        {
            // log finger
            double ts = GetUnixEpoch();
            string iId = item.Uid;

            string fLogLine = string.Format(FINGER_LINE_FORMAT, ts, (int)EventType.FINGER, fId, iId, action, x, y);
            WriteLine(fLogLine);

            // log puzzle
            double iX = item.Center.X;
            double iY = item.Center.Y;
            double iA = item.Orientation;
            double iS = item.Width * item.Height;

            PuzzlePiece pp;
            if (!Pieces.ContainsKey(iId))
            {
                pp = new PuzzlePiece(item);
            }
            else
            {
                pp = Pieces[iId];
            }

            double dX = iX - pp.x;
            double dY = iY - pp.y;
            double dA = iA - pp.angle;
            double dS = iS - pp.size;

            // if changed in any way
            if (Math.Abs(dX) > THRESH ||
                Math.Abs(dY) > THRESH ||
                Math.Abs(dA) > THRESH ||
                Math.Abs(dS) > THRESH)
            {
                //Console.WriteLine(dX);

                string pLogLine = string.Format(PUZZLE_LINE_FORMAT, ts, (int)EventType.PUZZLE, fId, iId, iX, iY, iA, iS, dX, dY, dA, dS);
                WriteLine(pLogLine);
            }

            pp.x = iX;
            pp.y = iY;
            pp.angle = iA;
            pp.size = iS;
            Pieces[iId] = pp;
        }

        public void LogJoin()
        {
            double ts = GetUnixEpoch();
            string jLogLine = string.Format(ACTIVITY_LINE_FORMAT, ts, (int)EventType.ACTIVITY, (int)ActivityType.JOIN);
            WriteLine(jLogLine);
        }

        public void LogCompleted()
        {
            double ts = GetUnixEpoch();
            string jLogLine = string.Format(ACTIVITY_LINE_FORMAT, ts, (int)EventType.ACTIVITY, (int)ActivityType.COMPLETED);
            WriteLine(jLogLine);
        }

        public void LogManipulation()
        {
            string manipulation = "scatter_OnManipulationCompleted called.";
            WriteLine(manipulation);
        }

        public void Save()
        {
            //TextWriter tr = new StreamWriter(FileName, true);
            //tr.Write(Log);
            //tr.Close();
        }

        /* Private */
        private double GetUnixEpoch()
        {
            TimeSpan t = (DateTime.UtcNow - new DateTime(1970, 1, 1, 0, 0, 0, 0).ToLocalTime());
            return t.TotalMilliseconds;
        }

        private void WriteLine(string line)
        {
            Log += line;
            Log += "\r\n";
        }

        public void LogAnalyzerSample(string id, double sample, double actualTime, double newVal, double newTime,
            double timesI, double timesJm1, double timesJ, double valsJm2, double valsJm1, double valsJ, double valsJp1, double J)
        {
            StreamWriter sw;
            switch (id)
            {
                case "xs":
                    sw = new StreamWriter(xsamples, true);
                    break;
                case "ys":
                    sw = new StreamWriter(ysamples, true);
                    break;
                case "imuX":
                    sw = new StreamWriter(imuxsamples, true);
                    break;
                default:
                    sw = new StreamWriter(imuysamples, true);
                    break;
            }
            sw.WriteLine(gestureNo + " " + sample + " " + actualTime + " " + newVal + " " + newTime + " " + timesI + " " + timesJm1 + " " + timesJ
                         + " " + valsJm2 + " " + valsJm1 + " " + valsJ + " " + valsJp1 + " " + J);
            sw.Close();
        }

        public void LogSample(string id, double sample, double actualTime, double newVal, double newTime)
        {
            StreamWriter sw;
            switch (id)
            {
                case "xs":
                    sw = new StreamWriter(xsamples, true);
                    break;
                case "ys":
                    sw = new StreamWriter(ysamples, true);
                    break;
                case "imuX":
                    sw = new StreamWriter(imuxsamples, true);
                    break;
                case "imuY":
                    sw = new StreamWriter(imuysamples, true);
                    break;
                default:
                    sw = new StreamWriter("ohoh.txt", true);
                    break;
            }
            string line = gestureNo + " " + sample + " " + actualTime + " " + newVal + " " + newTime;
            sw.WriteLine(line);
            sw.Close();
        }

        public void LogSample(double time, double x, double y, double iX, double iY)
        {
            StreamWriter sw = new StreamWriter("data.txt", true);
            sw.WriteLine(gestureNo + " " + time + " " + x + " " + y + " " + iX + " " + iY);
            sw.Close();
        }

        public void LogSample(double time, float ax, float ay, float az)
        {
            StreamWriter sw = new StreamWriter("acceldata.txt", true);
            sw.WriteLine(gestureNo + " " + time + " " + ax + " " + ay + " " + az);
            sw.Close();
        }

        public void LogTouchGesture(TouchGesture tg)
        {
            string filename = "log-touchGesture-" + comparisonIndex + ".txt";
            StreamWriter sw = new StreamWriter(filename);
            for (int i = 0; i < tg.gestureData.Count; i++)
            {
                sw.WriteLine(tg.gestureData[i].actualTime + " " + tg.gestureData[i].x + " " + tg.gestureData[i].y);
            }
            sw.Close();
        }

        public void LogIMUGesture(IMUGesture imug)
        {
            string filename = "log-imuGesture-" + comparisonIndex + ".txt";
            StreamWriter sw = new StreamWriter(filename);
            //sw.WriteLine(imug.indexOfGestureInList + " " + 0 + " " + 0 + " " + 0 + " " + 0 + " " + 0 + " " + 0);
            for (int i = 0; i < imug.gestureData.Count; i++)
            {
                sw.WriteLine(imug.gestureData[i].actualTime + " " + imug.gestureData[i].linearAcc[0] + " " + imug.gestureData[i].linearAcc[1] + " " + imug.gestureData[i].linearAcc[2] + " " + imug.gestureData[i].a[0] + " " + imug.gestureData[i].a[1] + " " + imug.gestureData[i].a[2]);
            }
            sw.Close();
        }

        public void LogEventIMUGesture(IMUGesture imug)
        {
            string filename = "log-EventimuGesture-" + comparisonIndex + ".txt";
            StreamWriter sw = new StreamWriter(filename);
            //sw.WriteLine(imug.indexOfGestureInList + " " + 0 + " " + 0 + " " + 0 + " " + 0 + " " + 0 + " " + 0);
            for (int i = 0; i < imug.gestureData.Count; i++)
            {
                sw.WriteLine(imug.gestureData[i].actualTime + " " + imug.gestureData[i].linearAcc[0] + " " + imug.gestureData[i].linearAcc[1] + " " + imug.gestureData[i].linearAcc[2] + " " + imug.gestureData[i].a[0] + " " + imug.gestureData[i].a[1] + " " + imug.gestureData[i].a[2]);
            }
            sw.Close();
        }

        public void LogXYsamples(double[] xs, double[] ys, double[] times, string tag)
        {
            string filename = "log-xySamples-" + tag + "-" + comparisonIndex + ".txt";
            StreamWriter sw = new StreamWriter(filename);
            for (int i = 0; i < xs.Length; i++)
            {
                sw.WriteLine(times[i] + " " + xs[i] + " " + ys[i]);
            }
            sw.Close();
        }

        public void LogDsamples(double[] d, double[] times, string tag)
        {
            string filename = "log-dSamples-" + tag + "-" + comparisonIndex + ".txt";
            StreamWriter sw = new StreamWriter(filename);
            for (int i = 0; i < d.Length; i++)
            {
                sw.WriteLine(times[i] + " " + d[i]);
            }
            sw.Close();
        }
    }
}
