function [ x, y, ix, iy ] = plotResamples( matrix )
%PLOT Summary of this function goes here
%   Detailed explanation goes here

figureCount = 1;

x = load('Xsamples.txt');
y = load('Ysamples.txt');
ix = load('imuXsamples.txt');
iy = load('imuYsamples.txt');
if (matrix(1,1) == 1)
    [ finalFigureCount ] = plotResample(figureCount, x, 'x');
    figureCount = finalFigureCount;
end
if (matrix(1,2) == 1)
    [ finalFigureCount ] = plotResample(figureCount, y, 'y');
    figureCount = finalFigureCount;
end
if (matrix(1,3) == 1)
    [ finalFigureCount ] = plotResample(figureCount, ix, 'imuX');
    figureCount = finalFigureCount;
end
if (matrix(1,4) == 1)
    [ finalFigureCount ] = plotResample(figureCount, iy, 'imuY');
end

end

function [ finalFigureCount ] = plotResample( figureCount, data, title )

currentGesture = data(1,1);

samples = [];
times = [];
nsamples = [];
ntimes = [];
for i=1:length(data(:,1))
    gesture = data(i,1);    
    if (gesture ~= currentGesture || i == length(data(:,1)))
        plotFunc(title, figureCount, currentGesture, samples, times, nsamples, ntimes);
        currentGesture = gesture;
        figureCount = figureCount + 1;
        samplesSorted = issorted(samples);
        timesSorted = issorted(times);
        nsamplesSorted = issorted(nsamples);
        ntimesSorted = issorted(ntimes);
        samples = [];
        times = [];
        nsamples = [];
        ntimes = [];
    else
        samples = [samples; data(i,2)];
        times = [times; data(i,3)];
        nsamples = [nsamples; data(i,4)];
        ntimes = [ntimes; data(i,5)];
    end
end

fprintf('%s\nsamplesSorted: %d\ntimesSorted: %d\nsamplesSorted: %d\nntimesSorted: %d\n', ...
    title, samplesSorted, timesSorted, nsamplesSorted, ntimesSorted);

finalFigureCount = figureCount;

end

function [] = plotFunc(ttl, fg, ges, samples, times, newSamples, newTimes)

str = [ttl ' ' num2str(ges)];

figure(fg);
plot(times, samples, 'r');
hold on;
title(str);
plot(newTimes, newSamples, 'b');

end