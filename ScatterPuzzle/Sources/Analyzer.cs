﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Media;
using System.ComponentModel;
using System.Threading;
using System.Windows.Input;
using System.Windows.Ink;
using System.IO;
using MathNet.Numerics.Interpolation.Algorithms;
using Microsoft.Surface.Presentation.Controls;

namespace Microsoft.Surface.Samples.ScatterPuzzle
{
    public class Analyzer
    {
        #region Public Members

        public bool initalisedOnline = false; // has the online initialisation completed?
        public int SENSOR_COUNT; // number of sensors connected to the application

        public const int QUEUE_SIZE = 2000; // the maximum number of values we want to keep in our queues
        public static int FILTER; // The filtering to perform on our magnitude signals

        // When a user touches down on a piece, we want to use the TouchDevice as a key to store both
        // the AnalyzerState which holds the data to identify the touch, and the ScatterViewItem (which
        // store the PuzzlePiece and will allow us to deal with the preceeding identification)
        public Dictionary<TouchDevice, AnalyzerState> touches;
        public Dictionary<TouchDevice, ScatterViewItem> puzzlePieces;

        #endregion

        #region Private Members

        // Reference to main window so we can call identification functions
        private Window1 parentWindow;

        // The background worker used to calculate the magnitude difference between signals at
        // regular intervals
        private BackgroundWorker corrBW;

        // The time for the analyzer to sleep in msc
        private static int _analyzerSleep = 500;
        private static int ANALYZER_SLEEP
        {
            get { return _analyzerSleep; }
            set
            {
                _analyzerSleep = value;
            }
        }

        #endregion
        #region Initialisation

        /// <summary>
        /// Default constructor for the analyzer. Simply needs a reference to the parent window
        /// </summary>
        /// <param name="parent">Reference to the Main Window</param>
        public Analyzer(Window1 parent)
        {
            parentWindow = parent;
        }

        /// <summary>
        /// Initialises the online component of the analyer. This must be called after the constructor
        /// as otherwise we would not know the number of sensors connecting to the application
        /// </summary>
        /// <param name="sensorCount">Number of sensors connected to the application</param>
        /// <param name="filterVal">The size of filtering to use on our signals</param>
        public void initialiseOnline(int sensorCount, int filterVal)
        {
            SENSOR_COUNT = sensorCount;
            FILTER       = filterVal;
            touches      = new Dictionary<TouchDevice, AnalyzerState>();
            puzzlePieces      = new Dictionary<TouchDevice, ScatterViewItem>();
            initalisedOnline = true;

            // Setup the background worker used to find the identification online
            corrBW = new BackgroundWorker();
            corrBW.WorkerSupportsCancellation = true;
            corrBW.DoWork += new DoWorkEventHandler(corrBW_DoWork);
            corrBW.ProgressChanged += new ProgressChangedEventHandler(corrBW_OnlineUpdate);
            corrBW.RunWorkerCompleted += new RunWorkerCompletedEventHandler(corrBW_RunWorkerCompleted);
            corrBW.WorkerReportsProgress = true;
            //corrBW.RunWorkerAsync();
        }
        void corrBW_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e) { }

        #endregion
        #region Online Analyzer Worker

        /// <summary>
        /// Calculates the magnitude difference for each touch signal at specified intervals
        /// </summary>
        void corrBW_DoWork(object sender, DoWorkEventArgs e)
        {
            int currentTime;
            int n = 1;
            int sleepTime;
            while (!corrBW.CancellationPending)
            {
                currentTime = (int)(System.DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond - Window1.startTime);
                // We only want to calculate the correlation if there's touches left to identify
                if (touchesLeftToIdentify() > 0)
                {
                    corrBW.ReportProgress(currentTime);
                    sleepTime = n * ANALYZER_SLEEP - currentTime;
                    while (sleepTime < 0)
                    {
                        n++;
                        sleepTime = n * ANALYZER_SLEEP - currentTime;
                    };
                    Thread.Sleep(sleepTime);
                    n++;
                }
            }
        }
        
        /// <summary>
        /// Calculates the magnitude difference between each sensor and their respective touch signals
        /// and then makes the call to identify the signals through the main window
        /// </summary>
        void corrBW_OnlineUpdate(object sender, ProgressChangedEventArgs e)
        {
            // Create an array to store the correlation values, and initialise it
            double[][] corD = new double[touches.Count][];
            for (int i = 0; i < corD.Length; i++) {
                corD[i] = new double[SENSOR_COUNT];
            }

            // For each touch event we want to calculate the magnitude difference
            int[] signalLength = new int[touches.Count];
            for (int i = 0; i < touches.Count; i++)
            {
                corD[i] = calculateMagnitudeDifference(i, out signalLength[i]);
            }

            // And then make the identification
            parentWindow.onlineIdentification(touches.Keys.ToArray(), corD, signalLength);
        }

        #endregion
        #region Difference Calculation

        /// <summary>
        /// Calculates the magnitude difference for each IMU signal for a specified touch signal
        /// </summary>
        /// <param name="touchIndex">The index of the touch signal to compare the IMU signals with</param>
        /// <param name="signalLength">Outputs the length of the signal in msec</param>
        /// <returns></returns>
        public double[] calculateMagnitudeDifference(int touchIndex, out int signalLength)
        {
            AnalyzerState state = touches.Values.ToArray()[touchIndex]; // the state being analysed
             
            // Create an array to store our magnitude difference (one value for each sensor)
            double[] magDiff = new double[SENSOR_COUNT];
            double[] times = state.times.ToArray(); // touch timestamps

            // Sets the signal length
            if (times.Length > 0) signalLength = (int)((times[times.Length - 1] * 1000) - (times[0] * 1000));
            else signalLength = 0;

            // Magnitude arrays
            double[][] imuLinDVals = new double[SENSOR_COUNT][]; // imus
            double[][] daccs = new double[SENSOR_COUNT][]; // touch

            // calculate the difference for each sensor
            for (int i = 0; i < SENSOR_COUNT; i++)
            {
                imuLinDVals[i] = state.imuLinDQueue[i].ToArray(); // get the linAcc mag. from the queue
                double range = 0; // not using the range, but need it anyway
                double[] imuTimes = state.imuTimes[i].ToArray();

                // Sometimes there's some IMU data points which have timestamps before the first touch
                // signal point, and sometimes there are IMU timestamps after the last touch signal point,
                // so we need to crop the signal in order to avoid interpolation issues
                cropIMUdataToFitTouchTimes(times, imuLinDVals[i], imuTimes, 
                    out imuLinDVals[i], out imuTimes);

                // Resample the touch x+y for the IMU's timestamps and then get the second derivative
                double[] xs = resampleSignal(state.xs.ToArray(), times, imuTimes);
                double[] ys = resampleSignal(state.ys.ToArray(), times, imuTimes);
                double[] xvels = getDerivatives(xs, imuTimes, out range);
                double[] yvels = getDerivatives(ys, imuTimes, out range);
                double[] xaccs = getDerivatives(xvels, imuTimes, out range);
                double[] yaccs = getDerivatives(yvels, imuTimes, out range);

                // calculate the magnitude of the touch signal
                daccs[i] = new double[xaccs.Length];
                for (int k = 0; k < daccs[i].Length; k++)
                {
                    daccs[i][k] = Math.Sqrt(Math.Pow(xaccs[k], 2) + Math.Pow(yaccs[k], 2));
                }

                // Some values are unknown so we need to account for incorrect acc. calculation
                // and then level the playing field for the IMU signal
                if (daccs[i].Length > 0) daccs[i][0] = 0;
                if (daccs[i].Length > 1) daccs[i][1] = 0;
                if (daccs[i].Length > 0) daccs[i][daccs[i].Length - 1] = 0;
                if (imuLinDVals[i].Length > 0) imuLinDVals[i][0] = 0;
                if (imuLinDVals[i].Length > 1) imuLinDVals[i][1] = 0;
                if (imuLinDVals[i].Length > 0) imuLinDVals[i][imuLinDVals[i].Length - 1] = 0;

                // If we have the same number of points we can calculate the difference
                if (daccs[i].Length == imuLinDVals[i].Length)
                {
                    // First remove some noise using filtering
                    daccs[i] = filterData(daccs[i], 5);
                    imuLinDVals[i] = filterData(imuLinDVals[i], 3);
                    
                    //TODO: touch and IMU signals are out of sync, so temporarily a delay is added
                    imuLinDVals[i] = addDelay(imuLinDVals[i], -14);

                    double sigma = 0;
                    magDiff[i] = magnitudeDifference(daccs[i], imuLinDVals[i], out sigma);
                }
            }

            // Plot the signals in the state's comparison window
            switch (imuLinDVals.Length)
            {
                case 2: state.drawGraphs(daccs[0], daccs[1], imuLinDVals[0], imuLinDVals[1]);
                    break;
                case 3: state.drawGraphs(daccs[0], daccs[1], daccs[2], imuLinDVals[0], 
                    imuLinDVals[1], imuLinDVals[2]);
                    break;
            }
            return magDiff;
        }

        /// <summary>
        /// Crops the IMU data to avoid problems occuring with the interpolation
        /// </summary>
        /// <param name="times">The timestamps of the touch signal</param>
        /// <param name="imuD">The IMU magnitude values</param>
        /// <param name="imuTimes">The IMU timestampls</param>
        /// <param name="imuDout">The cropped IMU magnitude values</param>
        /// <param name="imuTimesOut">The cropped IMU timestamps</param>
        private void cropIMUdataToFitTouchTimes(double[] times, double[] imuD, double[] imuTimes, 
            out double[] imuDout, out double[] imuTimesOut)
        {
            double firstTouchTime = times[0];
            double lastTouchTime = times[times.Length - 1];

            List<double> iVals = imuD.ToList();
            List<double> iTimes = imuTimes.ToList();
            // remove some of the imu data at the start
            int count = 0;
            for (int i = 0; i < iTimes.Count; i++)
            {
                count++;
                if (iTimes[i] < firstTouchTime)
                    continue;
                iTimes.RemoveRange(0, count);
                iVals.RemoveRange(0, count);
                break;
            }

            // remove some of the imu data at the end
            count = 0;
            for (int i = 0; i < iTimes.Count; i++)
            {
                if (iTimes[i] < lastTouchTime)
                    continue;
                iTimes.RemoveRange(i, (iTimes.Count - i));
                iVals.RemoveRange(i, (iVals.Count - i));
                break;
            }

            imuDout = iVals.ToArray();
            imuTimesOut = iTimes.ToArray();
        }

        /// <summary>
        /// Resamples the signal using the MathNet.Numerics CubicInterpolation class
        /// </summary>
        /// <param name="values">The known y points of the signal</param>
        /// <param name="times">The known x points of the signal</param>
        /// <param name="resampledTimes">The x points at which we would like new interpolated y values</param>
        /// <returns></returns>
        private double[] resampleSignal(double[] values, double[] times, double[] resampledTimes)
        {
            double[] resampledValues = new double[resampledTimes.Length];
            
            // check that they're in ascending order
            double min = times[0];
            int minIndex = 0;
            int minCounter = 0;
            for (int i = 1; i < times.Length; i++)
            {
                if (times[i] <= min)
                {
                    min = times[i];
                    minIndex = i;
                    minCounter++;
                }
            }
            // If we find a time lower than the minimum time, then the device reset it's timer in the
            // middle of this signal. As a result we must discard the old values 
            if (minCounter > 0)
            {
                List<double> vals = values.ToList();
                List<double> time = times.ToList();
                vals.RemoveRange(0, minIndex - 1);
                time.RemoveRange(0, minIndex - 1);
                values = vals.ToArray();
                times = time.ToArray();
            }

            CubicSplineInterpolation csi = new CubicSplineInterpolation(times, values);
            for (int i = 0; i < resampledTimes.Length; i++)
            {
                resampledValues[i] = csi.Interpolate(resampledTimes[i]);
            }
            return resampledValues;
        }

        #endregion
        #region Auxillary Calculations

        /// <summary>
        /// Calculates the difference between two signals by calculating the sum of the 
        /// differences of each point
        /// </summary>
        /// <param name="d1">The first signal</param>
        /// <param name="d2">The second signal</param>
        /// <param name="sigma">The standard deviation of the difference array</param>
        /// <returns></returns>
        public double magnitudeDifference(double[] d1, double[] d2, out double sigma)
        {
            // calculate the difference and find the total difference
            double[] diff = new double[d1.Length];
            double totalDifference = 0;
            for (int i = 0; i < diff.Length; i++)
            {
                diff[i] = Math.Abs(d2[i] - d1[i]);
                totalDifference += diff[i];
            }
            // find the mean
            double mean = totalDifference / diff.Length;

            // find the variance
            double[] squaredMeanDifference = new double[d1.Length];
            double total = 0;
            for (int i = 0; i < squaredMeanDifference.Length; i++)
            {
                squaredMeanDifference[i] = Math.Pow(diff[i] - mean, 2);
                total += squaredMeanDifference[i];
            }
            double variance = total / squaredMeanDifference.Length;

            // find the standard deviation
            sigma = Math.Sqrt(variance);

            return totalDifference;
        }

        /// <summary>
        /// Finds the rate of change between each two successive points 
        /// </summary>
        /// <param name="ds">The set of values for each point</param>
        /// <param name="times">The set of times for each point</param>
        /// <param name="valueRange">Outputs the range of calculated values</param>
        /// <returns></returns>
        double[] getDerivatives(double[] ds, double[] times, out double valueRange)
        {
            double min = double.MaxValue, max = double.MinValue;
            double[] vels = new double[ds.Length];
            for (int i = 0; i < ds.Length; i++)
            {
                if (i > 0)
                {
                    if (times[i] == times[i - 1])
                        vels[i] = vels[i - 1];
                    else
                        vels[i] = (ds[i] - ds[i - 1]) / (times[i] - times[i - 1]);
                }
                else
                    vels[i] = 0;
                if (vels[i] > max)
                    max = vels[i];
                if (vels[i] < min)
                    min = vels[i];
            }
            valueRange = max - min;
            return vels;
        }

        #endregion
        #region Filtering

        public static double[] filterData(double[] data, int nFilterPoints)
        {
            //This needs to be replaced with a proper general nPoints moving average filter
            //that takes into account the special characteristics of the touch surface signal
            //and the possibility of a small number of points.
            if (nFilterPoints == 0)
                return data;
            else if (nFilterPoints == 3)
                return filterData3(data);
            else
                return filterData5(data);
        }

        //This needs to be replaced with a proper general nPoints moving average filter
        //that takes into account the special characteristics of the touch surface signal
        //and the possibility of a small number of points.
        private static double[] filterData3(double[] data)
        {
            if(data.Length<=1)
                return data;
            double[] fd = new double[data.Length];
            fd[0] = (data[0]+data[1])/2;
            int i;
            for (i = 1; i < (data.Length-1); i++)
            {
                fd[i] = (data[i - 1] + data[i] + data[i + 1]) / 3;
            }
            fd[i] = (data[i - 1] + data[i]) / 2;

            return fd;
        }

        //This needs to be replaced with a proper general nPoints moving average filter
        //that takes into account the special characteristics of the touch surface signal
        //and the possibility of a small number of points.
        private static double[] filterData5(double[] data)
        {
            if (data.Length <= 4)
                return data;
            double[] fd = new double[data.Length];
            fd[0] =  (data[0] + data[1] + data[2]) / 3;
            fd[1] =  (data[0] + data[1] + data[2] + data[3]) / 4;
            int i;
            for (i = 2; i < (data.Length - 2); i++)
            {
                fd[i] = (data[i - 2] + data[i - 1] + data[i] + data[i + 1] + data[i + 2]) / 5;
            }
            fd[i] = (data[i - 2] + data[i - 1] + data[i]+data[i+1]) / 4;
            fd[i + 1] = (data[i - 1] + data[i] + data[i + 1]) / 3;

            return fd;
        }

        #endregion
        #region Auxillary Functions

        /// <summary>
        /// Retrieves the index of a touch based upon the TouchDevice key
        /// </summary>
        /// <param name="key">The key for the touches dictionary</param>
        /// <returns>The index of the AnalyzerState in the dictionar</returns>
        public int indexFromKey(TouchDevice key)
        {
            for (int i = 0; i < touches.Count; i++)
            {
                if (touches.Keys.ToArray()[i] == key)
                {
                    return i;
                }
            }
            return -1;
        }

        /// <summary>
        /// Retrieve the TouchDevice based upon the device ID
        /// </summary>
        /// <param name="deviceID">The device.ID value</param>
        /// <returns>The TouchDevice stored as a key in the touches dictionary</returns>
        public TouchDevice deviceFromDeviceID(int deviceID)
        {
            for (int i = 0; i < touches.Count; i++)
            {
                if (touches.Keys.ToArray()[i].Id == deviceID)
                    return touches.Keys.ToArray()[i];
            }
            return null;
        }

        /// <summary>
        /// Calculates the amount of touches/states in the array that still need to be identified
        /// </summary>
        /// <returns>The number of touches/states still left to be identified</returns>
        public int touchesLeftToIdentify()
        {
            int count = 0;
            for (int i = 0; i < touches.Count; i++)
            {
                try
                {
                    count += touches.Values.ToArray()[i].needsIdentity;
                }
                catch (IndexOutOfRangeException e)
                {
                    Console.WriteLine("IndexOutOfRangeException: " + e.StackTrace);
                }
                catch (NullReferenceException e)
                {
                    Console.WriteLine("NullReferenceException: " + e.StackTrace);
                }
                catch (ArgumentException e)
                {
                    Console.WriteLine("ArgumentException: " + e.StackTrace);
                }
            }
            return count;
        }

        /// <summary>
        /// Crops a queue to the specified queue size
        /// </summary>
        /// <param name="queue">The queue to crop</param>
        /// <param name="queueSize">The queue size to align the queue to</param>
        /// <returns></returns>
        public static Queue<double> shiftQueue(Queue<double> queue, int queueSize)
        {
            if (queue.Count <= queueSize)
                return queue;
            int overfill = queue.Count - queueSize;
            for (int i = 0; i < overfill; i++)
            {
                queue.Dequeue();
            }
            return queue;
        }

        /// <summary>
        /// Initialises each element of a queue array as a queue of doubles
        /// </summary>
        /// <param name="queueArray">The queue array to initialise</param>
        public static void initialiseQueueArray(Queue<double>[] queueArray)
        {
            for (int i = 0; i < queueArray.Length; i++)
            {
                queueArray[i] = new Queue<double>();
            }
        }

        /// <summary>
        /// Shifts a signal along by the specified amount
        /// </summary>
        /// <param name="y">The values in the signal</param>
        /// <param name="d">The delay to add to the signal</param>
        /// <returns></returns>
        public double[] addDelay(double[] y, int d)
        {
            double[] newY = new double[y.Length];

            for (int i = 0; i < y.Length; i++)
            {
                int j = i + d;
                if (j > 0 && j < y.Length)
                {
                    newY[j] = y[i];
                }
            }
            return newY;
        }

        #endregion
    }
}
