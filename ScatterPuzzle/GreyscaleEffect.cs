﻿using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Effects;

namespace Effects
{
    public class GreyscaleEffect : ShaderEffect
    {
        public GreyscaleEffect()
        {
            PixelShader = _pixelShader;
            UpdateShaderValue(InputProperty);
        }

        public Brush Input
        {
            get { return (Brush)GetValue(InputProperty); }
            set { SetValue(InputProperty, value); }
        }

        public static readonly DependencyProperty InputProperty =
            ShaderEffect.RegisterPixelShaderSamplerProperty(
                    "Input", 
                    typeof(GreyscaleEffect),
                    0);

        private static PixelShader _pixelShader =
            new PixelShader() { UriSource = Global.MakePackUri("GreyscaleEffect.ps") };

    }
}
