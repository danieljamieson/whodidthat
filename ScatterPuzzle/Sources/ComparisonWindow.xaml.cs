﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Microsoft.Surface.Samples.ScatterPuzzle
{
    /// <summary>
    /// Interaction logic for ComparisonWindow.xaml
    /// </summary>
    public partial class ComparisonWindow : Window
    {
        List<double[]> yStore = new List<double[]>();

        public ComparisonWindow()
        {
            InitializeComponent();
        }

        public void updateDelay(object sender, RoutedEventArgs e)
        {
            int newDelay = int.Parse(delayTB.Text);
            double[] y1 = addDelay(yStore[0], newDelay);
            double[] y2 = addDelay(yStore[1], newDelay);
            double[] y3 = addDelay(yStore[2], newDelay);

            drawGraphs(y1, y2, y3);
        }

        public double[] addDelay(double[] y, int d)
        {
            double[] newY = new double[y.Length];

            for (int i = 0; i < y.Length; i++)
            {
                int j = i + d;
                if (j > 0 && j < y.Length)
                {
                    newY[j] = y[i];
                }
            }
            return newY;
        }

        public void clearStores()
        {
            yStore.Clear();
        }

        public void updateResults(int delay, double corr)
        {
            corelationLbl.Content = "Correlation=" + corr;
        }

        public void drawGraphs(double[] y1, double[] y2, double[] y3, double[] y4, double[] y5, double[] y6)
        {
            graphsCanvas.Children.Clear();
            yStore.Clear();
            Polyline pl = createPath(Brushes.Pink, 2, graphsCanvas);
            Polyline p2 = createPath(Brushes.SeaGreen, 2, graphsCanvas);
            Polyline p3 = createPath(Brushes.AliceBlue, 2, graphsCanvas);
            Polyline p4 = createPath(Brushes.Red, 2, graphsCanvas);
            Polyline p5 = createPath(Brushes.Green, 2, graphsCanvas);
            Polyline p6 = createPath(Brushes.Blue, 2, graphsCanvas);

            int totalX = (int)graphsCanvas.RenderSize.Width;
            int xInterval = totalX / y1.Length;
            Point pt;
            double min = double.MaxValue;
            double max = double.MinValue;
            for (int x = 0, i = 0; i < y1.Length && i < y2.Length && i < y3.Length 
                && i < y4.Length && i < y5.Length && i < y6.Length; i++, x += xInterval)
            {
                if (y1 != null)
                {
                    if (y1[i] > max)
                        max = y1[i];
                    else if (y1[i] < min)
                        min = y1[i];
                }
                if (y2 != null)
                {
                    if (y2[i] > max)
                        max = y2[i];
                    else if (y2[i] < min)
                        min = y2[i];
                }
                if (y3 != null)
                {
                    if (y3[i] > max)
                        max = y3[i];
                    else if (y3[i] < min)
                        min = y3[i];
                }
                if (y4 != null)
                {
                    if (y4[i] > max)
                        max = y4[i];
                    else if (y4[i] < min)
                        min = y4[i];
                }
                if (y5 != null)
                {
                    if (y5[i] > max)
                        max = y5[i];
                    else if (y5[i] < min)
                        min = y5[i];
                }
                if (y6 != null)
                {
                    if (y6[i] > max)
                        max = y6[i];
                    else if (y6[i] < min)
                        min = y6[i];
                }
            }
            double yScale = 150 / (max - min);
            for (int x = 0, i = 0; i < y1.Length && i < y2.Length && i < y3.Length
                && i < y4.Length && i < y5.Length && i < y6.Length; i++, x += xInterval)
            {
                if (y1 != null)
                {
                    pt = new Point(x, 300 - (y1[i] * yScale));
                    //pt = new Point(x, y[i]) ;
                    pl.Points.Add(pt);
                }
                if (y2 != null)
                {
                    pt = new Point(x, 300 - (y2[i] * yScale));
                    p2.Points.Add(pt);
                }
                if (y3 != null)
                {
                    pt = new Point(x, 300 - (y3[i] * yScale));
                    p3.Points.Add(pt);
                }
                if (y4 != null)
                {
                    pt = new Point(x, 300 - (y4[i] * yScale));
                    p4.Points.Add(pt);
                }
                if (y5 != null)
                {
                    pt = new Point(x, 300 - (y5[i] * yScale));
                    p5.Points.Add(pt);
                }
                if (y6 != null)
                {
                    pt = new Point(x, 300 - (y6[i] * yScale));
                    p6.Points.Add(pt);
                }
            }
        }

        public void drawGraphs(double[] y1, double[] y2, double[] y3, double[] y4)
        {
            if (y1 == null || y2 == null || y3 == null || y4 == null)
                return;
            graphsCanvas.Children.Clear();
            yStore.Clear();
            Polyline pl = createPath(Brushes.Pink, 2, graphsCanvas);
            Polyline p2 = createPath(Brushes.SeaGreen, 2, graphsCanvas);
            Polyline p3 = createPath(Brushes.Red, 2, graphsCanvas);
            Polyline p4 = createPath(Brushes.Green, 2, graphsCanvas);

            int totalX = (int)graphsCanvas.RenderSize.Width;
            int xInterval = totalX / y1.Length;
            Point pt;
            double min = double.MaxValue;
            double max = double.MinValue;
            for (int x = 0, i = 0; i < y1.Length && i < y2.Length && i < y3.Length && i < y4.Length; i++, x += xInterval)
            {
                if (y1[i] > max)
                    max = y1[i];
                else if (y1[i] < min)
                    min = y1[i];

                if (y2[i] > max)
                    max = y2[i];
                else if (y2[i] < min)
                    min = y2[i];

                if (y3[i] > max)
                    max = y3[i];
                else if (y3[i] < min)
                    min = y3[i];

                if (y4[i] > max)
                    max = y4[i];
                else if (y4[i] < min)
                    min = y4[i];
            }
            double yScale = 150 / (max - min);
            for (int x = 0, i = 0; i < y1.Length && i < y2.Length && i < y3.Length && i < y4.Length; i++, x += xInterval)
            {
                pt = new Point(x, 300 - (y1[i] * yScale));
                //pt = new Point(x, y[i]) ;
                pl.Points.Add(pt);

                pt = new Point(x, 300 - (y2[i] * yScale));
                p2.Points.Add(pt);

                pt = new Point(x, 300 - (y3[i] * yScale));
                p3.Points.Add(pt);

                pt = new Point(x, 300 - (y4[i] * yScale));
                p4.Points.Add(pt);
            }
            yStore.Add(y1);
            yStore.Add(y2);
            yStore.Add(y2);
            yStore.Add(y3);
        }

        public void drawGraphs(double[] y1, double[] y2, double[] y3)
        {
            if (y1 == null || y2 == null || y3 == null)
                return;
            //if (y1.Length != y2.Length || y1.Length != y3.Length || y2.Length != y3.Length)
            //    return;
            graphsCanvas.Children.Clear();
            yStore.Clear();
            Polyline pl = createPath(Brushes.Black, 2, graphsCanvas);
            Polyline p2 = createPath(Brushes.Red, 2, graphsCanvas);
            Polyline p3 = createPath(Brushes.Green, 2, graphsCanvas);

            int totalX = (int)graphsCanvas.RenderSize.Width;
            int xInterval = totalX / y1.Length;
            Point pt;
            double min = double.MaxValue;
            double max = double.MinValue;
            for (int x = 0, i = 0; i < y1.Length && i < y2.Length && i < y3.Length; i++, x += xInterval)
            {
                if (y1[i] > max)
                    max = y1[i];
                else if (y1[i] < min)
                    min = y1[i];

                if (y2[i] > max)
                    max = y2[i];
                else if (y2[i] < min)
                    min = y2[i];

                if (y3[i] > max)
                    max = y3[i];
                else if (y3[i] < min)
                    min = y3[i];
            }
            double yScale = 150 / (max - min);
            for (int x = 0, i = 0; i < y1.Length && i < y2.Length && i < y3.Length; i++, x += xInterval)
            {
                pt = new Point(x, 300 - (y1[i] * yScale));
                //pt = new Point(x, y[i]) ;
                pl.Points.Add(pt);

                pt = new Point(x, 300 - (y2[i] * yScale));
                p2.Points.Add(pt);

                pt = new Point(x, 300 - (y3[i] * yScale));
                p3.Points.Add(pt);
            }
            yStore.Add(y1);
            yStore.Add(y2);
            yStore.Add(y2);
        }

        public void drawGraph(double[] y, int graphIndex, bool clearCanvas)
        {
            if (clearCanvas)
            {
                graphsCanvas.Children.Clear();
            }
            if (y == null)
                return;
            yStore.Add(y);
            Brush brsh;
            if (graphIndex == 0)
                brsh = Brushes.Black;
            else if (graphIndex == 1)
                brsh = Brushes.Red;
            else
                brsh = Brushes.Green;
            Polyline pl = createPath(brsh, 2, graphsCanvas);

            int totalX = (int)graphsCanvas.RenderSize.Width;
            int xInterval = totalX / y.Length;
            Point pt;
            double min = double.MaxValue;
            double max = double.MinValue;
            for (int x = 0, i = 0; i < y.Length; i++, x += xInterval)
            {
                if (y[i] > max)
                    max = y[i];
                else if (y[i] < min)
                    min = y[i];
            }
            double yScale = 150 / (max - min);
            for (int x = 0, i = 0; i < y.Length; i++, x += xInterval)
            {
                pt = new Point(x, 300 - (y[i] * yScale));
                //pt = new Point(x, y[i]) ;
                pl.Points.Add(pt);
            }
        }

        private Polyline createPath(Brush brsh, int thickness, Canvas parentCanvas)
        {
            Polyline pl = new Polyline();
            parentCanvas.Children.Add(pl);
            pl.Stretch = Stretch.None;
            pl.StrokeEndLineCap = PenLineCap.Round;
            pl.StrokeLineJoin = PenLineJoin.Round;
            Canvas.SetLeft(pl, 0);
            Canvas.SetTop(pl, 0);
            pl.Stroke = brsh;
            pl.StrokeThickness = thickness;
            return pl;
        }
    }
}
