﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Microsoft.Surface.Samples.ScatterPuzzle
{
    public class AnalyzerState
    {
        public int needsIdentity; // does this state need identifying?

        // Reference to parent window to share public variables (such as DPI)
        Window1 parentWindow;
        // The comparison window used to graph the touch and IMU signals associated with this state
        ComparisonWindow cmp;

        public Queue<double> daccs; // magnitude of touch signal
        public Queue<double> times; // timestamps of the touch signal
        public Queue<double> xs; // touch x coordinate samples
        public Queue<double> ys; // touch y coordinate samples
        public Queue<double> xvels; // touch x velocity samples
        public Queue<double> yvels; // touch y velocity samples

        public Queue<double>[] imuTimes; // the timestamps of the IMU data
        public Queue<double>[] imuLinDQueue; // the magnitude of the IMU signal
        public Queue<double>[] imuLinXQueue; // the linear acceleration x samples
        public Queue<double>[] imuLinYQueue; // the linear acceleration y samples

        /// <summary>
        /// Constructs an AnalyzerState with a comparison window for plotting the signals associated
        /// with this state
        /// </summary>
        /// <param name="parent">The main window of the ScatterPuzzle</param>
        /// <param name="compareWin">The comparison window used to plot the signals</param>
        /// <param name="sensorCount">The number of sensors connected to the application</param>
        public AnalyzerState(Window1 parent, ComparisonWindow compareWin, int sensorCount)
        {
            cmp = compareWin;
            needsIdentity = 1;
            parentWindow = parent;
            daccs = new Queue<double>();
            times = new Queue<double>();
            xs = new Queue<double>();
            ys = new Queue<double>();
            xvels = new Queue<double>();
            yvels = new Queue<double>();
            imuTimes = new Queue<double>[sensorCount];
            imuLinDQueue = new Queue<double>[sensorCount];
            imuLinXQueue = new Queue<double>[sensorCount];
            imuLinYQueue = new Queue<double>[sensorCount];

            // Initialise each queue array so that we're ready to insert samples into them
            Analyzer.initialiseQueueArray(imuTimes);
            Analyzer.initialiseQueueArray(imuLinDQueue);
            Analyzer.initialiseQueueArray(imuLinXQueue);
            Analyzer.initialiseQueueArray(imuLinYQueue);
        }

        /// <summary>
        /// Shifts the times, xs and ys queues
        /// </summary>
        public void shiftQueues()
        {
            times = Analyzer.shiftQueue(times, Analyzer.QUEUE_SIZE);
            xs = Analyzer.shiftQueue(xs, Analyzer.QUEUE_SIZE);
            ys = Analyzer.shiftQueue(ys, Analyzer.QUEUE_SIZE);
        }

        /// <summary>
        /// Used to add IMU samples to the queues of this state
        /// </summary>
        /// <param name="i">The index of the sensor's queue which we'd like to add samples to</param>
        /// <param name="imuDataPoints">The data points to add to the queue's array</param>
        public void addIMUSamples(int i, List<IMUData> imuDataPoints)
        {
            IMUData[] ptList = imuDataPoints.ToArray();
            for (int j = 0; j < ptList.Length; j++)
            {
                // For each data point we want to add the linear acc x and y to our queues
                // and then add the timestamp of the data point to our timestamp queue
                imuTimes[i].Enqueue(ptList[j].actualTime);
                imuLinXQueue[i].Enqueue(ptList[j].linearAcc[0]);
                imuLinYQueue[i].Enqueue(ptList[j].linearAcc[2]);
                
                // Make sure we keep the queues within the queue size that we've defined
                imuTimes[i] = Analyzer.shiftQueue(imuTimes[i], Analyzer.QUEUE_SIZE);
                imuLinXQueue[i] = Analyzer.shiftQueue(imuLinXQueue[i], Analyzer.QUEUE_SIZE);
                imuLinYQueue[i] = Analyzer.shiftQueue(imuLinYQueue[i], Analyzer.QUEUE_SIZE);

                // If we have more than 0 values in the queue then we can start calculating magnitude
                if (imuLinXQueue[i].Count > 0 && imuLinYQueue[i].Count > 0)
                {
                    // Get the last linear acc x and y added to the queue and find the magnitude
                    double x = imuLinXQueue[i].ToArray()[imuLinXQueue[i].Count - 1];
                    double y = imuLinYQueue[i].ToArray()[imuLinYQueue[i].Count - 1];
                    double newImuLinDVal = Math.Sqrt(Math.Pow(x, 2) + Math.Pow(y, 2));

                    // Add the magnitude value to our queue and then keep it within our specified queue size
                    imuLinDQueue[i].Enqueue(newImuLinDVal);
                    imuLinDQueue[i] = Analyzer.shiftQueue(imuLinDQueue[i], Analyzer.QUEUE_SIZE);
                }
            }
        }

        /// <summary>
        /// Adds a touch sample to the state
        /// </summary>
        /// <param name="pt"></param>
        public void addTouchSample(TouchPoint pt)
        {
            // Firt add the timestamp to the time queue
            times.Enqueue(pt.actualTime);

            // Then we need to calculate the x and y coordinates in metres and add them to our queues
            double tx = pt.x;
            double ty = pt.y;
            double actualX = tx / parentWindow.dpiX; actualX *= 0.0254;
            double actualY = ty / parentWindow.dpiY; actualY *= 0.0254;
            xs.Enqueue(actualX);
            ys.Enqueue(actualY);

            shiftQueues(); // make sure the queues stay within our specified queue size
        }

        /// <summary>
        /// Called when this state has been identified and we want to stop this state from being
        /// considered by the analyzer
        /// </summary>
        public void Identified()
        {
            this.needsIdentity = 0;
        }

        //TODO: Could probably remove these drawGraphs functions and replace it with one function
        // which works no matter how many touch/IMU signals - for now it will be left for later work

        /// <summary>
        /// Plots a touch signal in comparison with two IMU signals - used when no interpolation
        /// is performed on the touch signal
        /// </summary>
        /// <param name="dacc">Magnitude of touch signal</param>
        /// <param name="sensor1">Magnitude of first sensor's signal</param>
        /// <param name="sensor2">Magnitude of second sensor's signal</param>
        public void drawGraphs(double[] dacc, double[] sensor1, double[] sensor2)
        {
            if (dacc.Length > 0 && sensor1.Length > 0 && sensor2.Length > 0)
            {
                cmp.drawGraphs(dacc, sensor1, sensor2);
            }
        }

        /// <summary>
        /// Plots two touch signals in comparison with two IMU signals - this is used when interpolation
        /// is performed on the original touch signal. Due to the timestamps for each sensor not being
        /// identical, it means we have two versions of the same touch signal - one for each set of
        /// timestamps
        /// </summary>
        /// <param name="dacc1">Magnitude of first interpolated touch signal</param>
        /// <param name="dacc2">Magnitude of second interpolated touch signal</param>
        /// <param name="sensor1">Magnitude of first IMU signal</param>
        /// <param name="sensor2">Magnitude of second IMU signal</param>
        public void drawGraphs(double[] dacc1, double[] dacc2, double[] sensor1, double[] sensor2)
        {
            if (dacc1.Length > 0 && dacc2.Length > 0 && sensor1.Length > 0 && sensor2.Length > 0)
            {
                cmp.drawGraphs(dacc1, dacc2, sensor1, sensor2);
            }
        }
        
        /// <summary>
        /// Plots three touch signals in comparison with three IMU signals - this is used when interpolation
        /// is performed AND we have three sensors instead of two.
        /// </summary>
        /// <param name="dacc1">Magnitude of first interpolated touch signal</param>
        /// <param name="dacc2">Magnitude of second interpolated touch signal</param>
        /// <param name="dacc3">Magnitude of third interpolated touch signal</param>
        /// <param name="sensor1">Magnitude of first IMU signal</param>
        /// <param name="sensor2">Magnitude of second IMU signal</param>
        /// <param name="sensor3">Magnitude of three IMU signal</param>
        public void drawGraphs(double[] dacc1, double[] dacc2, double[] dacc3,
            double[] sensor1, double[] sensor2, double[] sensor3)
        {
            if (dacc1.Length > 0 && dacc2.Length > 0 && dacc3.Length > 0 &&
                sensor1.Length > 0 && sensor2.Length > 0 && sensor3.Length > 0)
            {
                cmp.drawGraphs(dacc1, dacc2, dacc3, sensor1, sensor2, sensor3);
            }
        }
    }
}
