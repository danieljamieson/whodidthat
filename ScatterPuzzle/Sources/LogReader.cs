﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Windows.Controls;

namespace Microsoft.Surface.Samples.ScatterPuzzle
{
    class LogReader
    {
        public Dictionary<String,TouchGesture> allTouchGestures;
        public Dictionary<String, IMUGesture> allIMUGestures;
        public void readTabletopGesturesLog(StreamReader sr)
        {
            allTouchGestures = new Dictionary<String, TouchGesture>();
            String line;
            line = sr.ReadLine(); //header line
            TouchGesture touchGesture=null;
            TouchPoint touchPoint;
            int lastGestureNo = -1;
            do
            {
                line = sr.ReadLine();
                touchPoint = TouchPoint.read(line);
                if (lastGestureNo==-1 || lastGestureNo!=touchPoint.gestureNo)
                {
                    touchGesture = new TouchGesture();
                    touchGesture.gestureID = touchPoint.gestureID;
                    touchGesture.gestureNo = touchPoint.gestureNo;
                    touchGesture.uniqueID = touchGesture.gestureID+"-"+touchGesture.gestureNo;
                    allTouchGestures.Add(touchGesture.uniqueID , touchGesture);
                    lastGestureNo = touchGesture.gestureNo;
                }
                touchGesture.addTouchPoint(touchPoint);
            } while (!sr.EndOfStream);

        }

        public void readIMUGesturesLog(StreamReader sr)
        {
            allIMUGestures = new Dictionary<String,IMUGesture>();
            String line;
            line = sr.ReadLine(); //header line
            IMUGesture imuGesture=null;
            IMUData imuPoint;
            int lastGestureNo = -1;
            do
            {
                line = sr.ReadLine();
                imuPoint = IMUData.read(line);
                if (lastGestureNo == -1 || lastGestureNo != imuPoint.gestureNo)
                {
                    imuGesture = new IMUGesture();
                    imuGesture.gestureID = imuPoint.gestureID;
                    imuGesture.gestureNo = imuPoint.gestureNo;
                    imuGesture.uniqueID = imuGesture.gestureID+"-"+imuGesture.gestureNo;
                    allIMUGestures.Add(imuGesture.uniqueID , imuGesture);
                    lastGestureNo = imuGesture.gestureNo;
                }
                imuGesture.addIMUPoint(imuPoint);

            } while (!sr.EndOfStream);
        }
    }
}
