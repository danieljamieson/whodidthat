﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Microsoft.Surface.Samples.ScatterPuzzle
{
    class GestureMatchingInfo
    {
        public double matchPercentage;
        public int imuGestureIndex;
        public double calibAngle;
        public int delay;
        public GestureMatchingInfo(double percent, int i, double angle, int d)
        {
            matchPercentage = percent;
            imuGestureIndex = i;
            calibAngle = angle;
            delay = d;
        }

        public static GestureMatchingInfo[] getSortedArray(List<GestureMatchingInfo> matches)
        {
            GestureMatchingInfo[] sortedMatches = new GestureMatchingInfo[matches.Count];
            matches.CopyTo(sortedMatches);
            GestureMatchingInfo tmp;
            for (int i = 0; i < sortedMatches.Length - 1; i++)
            {
                for (int j = i + 1; j < sortedMatches.Length; j++)
                {
                    if (sortedMatches[i].matchPercentage < sortedMatches[j].matchPercentage)
                    {
                        tmp = sortedMatches[i];
                        sortedMatches[i] = sortedMatches[j];
                        sortedMatches[j] = tmp;
                    }
                }
            }
            return sortedMatches;
        }
    }
}
