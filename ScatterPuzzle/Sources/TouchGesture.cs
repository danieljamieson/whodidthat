﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Microsoft.Surface.Samples.ScatterPuzzle
{
    public struct TouchPoint
    {
        public string gestureID;
        public int gestureNo;
        public double unifiedTime,
                    actualTime;
        public int x, y;
        //public double accX;
        //public double accY;

        public void print()
        {
            Console.WriteLine("x: " + x + ", y: " + y);
        }

        public static TouchPoint read(String str)
        {
            TouchPoint td = new TouchPoint();
            string[] words = str.Split(',');
            td.gestureID = words[0];
            td.gestureNo = Int32.Parse(words[1]);
            td.unifiedTime = long.Parse(words[2]);
            td.actualTime = long.Parse(words[3]);
            td.x = int.Parse(words[4]);
            td.y = int.Parse(words[5]);
            return td;
        }

        public static TouchPoint newPoint(double x, double y)
        {
            TouchPoint tp = new TouchPoint();
            tp.x = (int)x; tp.y = (int)y;
            return tp;
        }

        public static TouchPoint newPoint(double x, double y, long time)
        {
            TouchPoint tp = new TouchPoint();
            tp.x = (int)x; tp.y = (int)y;
            tp.actualTime = time;
            tp.unifiedTime = time;
            return tp;
        }
    }
    public class TouchGesture
    {
        public string uniqueID; //gestureID+"-"+gestureNo;
        public string gestureID;
        public int gestureNo;
        public List<TouchPoint> gestureData;

        public int[] indexOfGestureInList;

        public TouchGesture()
        {
            gestureData = new List<TouchPoint>();
        }
        public void addTouchPoint(TouchPoint tp) {
            gestureData.Add(tp);
        }
        public double getRelativeTime(int i)
        {
            return gestureData[i].actualTime - gestureData[0].actualTime;
        }

        public void print()
        {
            Console.WriteLine("Printing touch gesture");
            foreach (TouchPoint tp in gestureData)
            {
                tp.print();
            }
        }
    }
}
