﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Threading;
using System.Windows.Input;
using System.IO;
using System.IO.Ports;
using System.Windows.Shapes;
using System.Windows.Media;
using System.Windows;
using System.Windows.Controls;
using Microsoft.Surface.Presentation.Controls;

namespace Microsoft.Surface.Samples.ScatterPuzzle
{
    public class LoggingThread
    {
        Analyzer analyzer;

        Logger logger;
        public Dictionary<PuzzlePiece, TouchGesture> activePieces;
        public List<CWASensor> cwas = new List<CWASensor>();

        private Window1 parentWindow;
        public List<TouchGesture> touchesList;

        private BackgroundWorker[] cwaLogger; // threading that logs data
        private Dictionary<int, int> threadSensorID = new Dictionary<int, int>();
        private BackgroundWorker touchLogger;

        ComparisonWindow touch1 = new ComparisonWindow();
        ComparisonWindow touch2 = new ComparisonWindow();

        private static int _cwaSleep = 10; //in msec
        private static int _touchSleep = 25;

        public static int TIMESCALE = 10000;
        private static int LOG_TIME_IN_SEC = 20;
        private static int DATA_LOG_SIZE = LOG_TIME_IN_SEC * 1000 / CWA_SLEEP;    //12.5 seconds 20 logs per second

        public static int CWA_SLEEP
        {
            get { return _cwaSleep; }
            set
            {
                _cwaSleep = value;
                DATA_LOG_SIZE = LOG_TIME_IN_SEC * 1000 / _cwaSleep;    //12.5 seconds 20 logs per second
            }
        }
        public static int TOUCH_SLEEP
        {
            get { return _touchSleep; }
            set
            {
                _touchSleep = value;
                DATA_LOG_SIZE = LOG_TIME_IN_SEC * 1000 / _touchSleep;    //12.5 seconds 20 logs per second
            }
        }

        public LoggingThread(Window1 pw, Logger lg, Analyzer _analyzer)
        {
            parentWindow = pw;
            logger = lg;
            analyzer = _analyzer;
            activePieces = new Dictionary<PuzzlePiece, TouchGesture>();
            touch1.Show();
            touch2.Show();
        }

        public void StartLogging()
        {
            // Set up thread to poll sensors
            cwaLogger = new BackgroundWorker[Window1.SENSOR_COUNT];

            for (int i = 0; i < Window1.SENSOR_COUNT; i++)
            {
                cwaLogger[i] = new BackgroundWorker();
                cwaLogger[i].WorkerSupportsCancellation = true;
                cwaLogger[i].DoWork += new DoWorkEventHandler(cwaLogger_DoWork);
                cwaLogger[i].RunWorkerCompleted += new RunWorkerCompletedEventHandler(cwaLogger_Completed);
                cwaLogger[i].ProgressChanged += new ProgressChangedEventHandler(cwaLogger_ProgressChanged);
                cwaLogger[i].WorkerReportsProgress = true;
                cwaLogger[i].RunWorkerAsync();
                threadSensorID.Add(cwaLogger[i].GetHashCode(), i);
            }

            // Setup thread to poll touch X and Y
            touchLogger = new BackgroundWorker();
            touchLogger.WorkerSupportsCancellation = true;
            touchLogger.DoWork += new DoWorkEventHandler(touchLogger_DoWork);
            touchLogger.RunWorkerCompleted += new RunWorkerCompletedEventHandler(touchLogger_Completed);
            touchLogger.ProgressChanged += new ProgressChangedEventHandler(touchLogger_ProgressChanged);
            touchLogger.WorkerReportsProgress = true;
            touchLogger.RunWorkerAsync(0);
        }

        #region TouchUpDown funcs

        /// <summary>
        /// Called by the main window when a touch down event is recorded on the InkCanvas. A new touchGesture is
        /// created for this new touch event, and the initial point is added
        /// </summary>
        /// <param name="touchDevice">The touch event specific device which collects the touch points</param>
        public void touchDown(TouchDevice touchDevice, ScatterViewItem item)
        {
            System.Windows.Input.TouchPoint pt = touchDevice.GetTouchPoint(parentWindow);
            //Console.WriteLine("x= " + pt.Position.X + ", y= " + pt.Position.Y);
            if (!analyzer.touches.ContainsKey(touchDevice))
            {
                //ONLINE
                if (analyzer.touches.Count == 0)
                    analyzer.touches.Add(touchDevice, new AnalyzerState(parentWindow, touch1, Window1.SENSOR_COUNT));
                else
                    analyzer.touches.Add(touchDevice, new AnalyzerState(parentWindow, touch2, Window1.SENSOR_COUNT));
            }
            if (!analyzer.puzzlePieces.ContainsKey(touchDevice))
            {
                analyzer.puzzlePieces.Add(touchDevice, item);
            }
        }

        /// <summary>
        /// Called by the main window when a touch up event is recorded on the InkCanvas. Retieves the relevant
        /// touch gesture and calls the Analyzer to correlate the touch and IMU data
        /// </summary>
        /// <param name="touchDevice">The touch event specific device which collects the touch points</param>
        public void touchUp(TouchDevice touchDevice)
        {
            //SUMDIFF
            int timestamp = (int)((DateTime.Now.Ticks / TIMESCALE) - Window1.startTime);
            TouchDevice touchDownDevice = analyzer.deviceFromDeviceID(touchDevice.Id);
            if (touchDownDevice != null)
            {
                if (analyzer.touches[touchDownDevice].needsIdentity != 0)
                {
                    int identifiedSensorID = 0;
                    int i = analyzer.indexFromKey(touchDownDevice);
                    double minDiff = 0;
                    double[] corD;
                    //if (i >= 0)
                    //{
                        int signalLength;
                        corD = analyzer.calculateMagnitudeDifference(i, out signalLength);
                        minDiff = double.MaxValue;
                        Console.WriteLine("touch signal= " + i + ", signalLength: " + signalLength);
                        for (int j = 0; j < corD.Length; j++)
                        {
                            Console.WriteLine(i + ", " + j + ": diff- " + corD[j]);
                            if (corD[j] < minDiff)
                            {
                                minDiff = corD[j];
                                identifiedSensorID = j;
                            }
                        }
                    //}
                    parentWindow.identifySensor(touchDownDevice, identifiedSensorID, corD, signalLength);
                }

                analyzer.touches.Remove(touchDownDevice);
                analyzer.puzzlePieces.Remove(touchDownDevice);
            }
        }

        #endregion


        #region TouchLogger stuff

        /// <summary>
        /// This function polls the touch screen
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void touchLogger_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            double time = e.ProgressPercentage / 1000.0;
            //Console.WriteLine(time);

            foreach (TouchDevice device in analyzer.touches.Keys)
            {
                System.Windows.Input.TouchPoint initialPoint = device.GetTouchPoint(parentWindow);
                Point touchPointAsPoint = new Point(initialPoint.Position.X, initialPoint.Position.Y);

                Point screenSpacePoint = parentWindow.PointToScreen(touchPointAsPoint);
                TouchPoint tgp = TouchPoint.newPoint(screenSpacePoint.X, screenSpacePoint.Y);
                //Console.WriteLine("touch" + i + "- X= " + tp.Position.X + ", Y= " + tp.Position.Y);

                tgp.unifiedTime = time;
                tgp.actualTime = time;

                AnalyzerState state = analyzer.touches[device];
                if (state.needsIdentity == 1)
                {
                    state.addTouchSample(tgp);
                }
            }
        }

        void touchLogger_DoWork(object sender, DoWorkEventArgs e)
        {
            int currentTime;
            int n = 1;
            int sleepTime;
            while (!touchLogger.CancellationPending)
            {
                currentTime = (int)((System.DateTime.Now.Ticks / TIMESCALE) - Window1.startTime);
                touchLogger.ReportProgress(currentTime);
                sleepTime = n * TOUCH_SLEEP - currentTime;
                while (sleepTime < 0)
                {
                    n++;
                    sleepTime = n * TOUCH_SLEEP - currentTime;
                };
                Thread.Sleep(sleepTime);
                n++;
            }
            Console.WriteLine("touch logging thread cancelled");
        }

        void touchLogger_Completed(object sender, RunWorkerCompletedEventArgs e)
        {
            Console.WriteLine("touch logger completed");
        }

        #endregion

        #region CWA Logger

        /// <summary>
        /// This function is called on a seperate thread and polls the Sensor and 
        /// the touch screen for new samples
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void cwaLogger_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            double time = e.ProgressPercentage / 1000.0;
            int me = threadSensorID[(sender as BackgroundWorker).GetHashCode()];

            CWASensor cwa = cwas[me];
            List<IMUData> imuData = new List<IMUData>();

            // Try and retrieve as many samples as possible
            IMUData? dp = null;
            do
            {
                dp = cwa.PollPort();
                if (dp.HasValue)
                {
                    IMUData dp2 = dp.Value;

                    if (Math.Abs((cwa.DeviceTime - DateTime.Now).TotalMilliseconds) > 1000)
                    {
                        Console.WriteLine("Device time reset.");
                        cwa.ResetDeviceTime();
                    }

                    dp2.actualTime = ((cwa.DeviceTime.Ticks / TimeSpan.TicksPerMillisecond) - Window1.startTime) / 1000.0;
                    dp2.unifiedTime = time;
                    imuData.Add(dp2);

                    cwa.UpdateDeviceTime();
                }
            } while (dp.HasValue);

            // Now just need to add them to analyer state that hasn't been identified
            foreach (AnalyzerState state in analyzer.touches.Values)
            {
                if (state.needsIdentity == 1)
                {
                    state.addIMUSamples(me, imuData);
                }
            }
        }

        void cwaLogger_DoWork(object sender, DoWorkEventArgs e)
        {
            int me = threadSensorID[(sender as BackgroundWorker).GetHashCode()];
            int currentTime;
            int n = 1;
            int sleepTime;
            while (!cwaLogger[me].CancellationPending)
            {
                currentTime = (int)(System.DateTime.Now.Ticks / TIMESCALE - Window1.startTime);
                cwaLogger[me].ReportProgress(currentTime);
                sleepTime = n * CWA_SLEEP - currentTime;
                while (sleepTime < 0)
                {
                    n++;
                    sleepTime = n * CWA_SLEEP - currentTime;
                };
                Thread.Sleep(sleepTime);
                n++;
            }
        }

        void cwaLogger_Completed(object sender, RunWorkerCompletedEventArgs e)
        {
            int me = threadSensorID[(sender as BackgroundWorker).GetHashCode()];
            Console.WriteLine("cwa logger + " + me + " completed");
        }

        #endregion

        /// <summary>
        /// Checks to see whether polling both sensors was successful
        /// </summary>
        /// <param name="success"></param>
        /// <returns></returns>
        private bool successfulPolling(int[] success)
        {
            int total = 0;
            for (int i = 0; i < success.Length; i++)
            {
                total += success[i];
            }
            if (total == success.Length)
                return true;
            else
                return false;
        }
    }
}
