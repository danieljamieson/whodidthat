﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.IO.Ports;
using System.ComponentModel;
using System.Windows;
using System.Text.RegularExpressions;

namespace Microsoft.Surface.Samples.ScatterPuzzle
{
    public class CWASensor
    {
        private int totalAttempts = 0;
        private int droppedSamples = 0;
        public static float PI = 3.14159265359f;

        // HACK: Sort out nicely
        public DateTime DeviceTime { get; protected set; }
        public void UpdateDeviceTime()
        {
            DeviceTime += TimeSpan.FromMilliseconds(10);
        }
        public void ResetDeviceTime()
        {
            DeviceTime = DateTime.Now;
        }

        private SerialPort port;
        private readonly BackgroundWorker portWorker = new BackgroundWorker(); // Performs connection work asynchronously
        public bool initialised = false;
        private string COM_PORT = "COM24";

        private List<byte> data_buffer = new List<byte>();

        // For calculating quaternions from acceleration and gyro
        private MadgwickAHRS ahrs;

        // For sampling via pattern matching the stream of data
        private Regex rx;
        const string SAMPLE_PATTERN = @"(?<ax>-?\d+),(?<ay>-?\d+),(?<az>-?\d+),(?<gx>-?\d+),(?<gy>-?\d+),(?<gz>-?\d+)";
        const float A_DIV = 4096.0f;    // Divide accelerometer data to get correct units
        const float G_MUL = 0.07f;      // Multiply gyroscope data to get correct units
        
        public CWASensor(string port)
        {
            ahrs    = new MadgwickAHRS();
            rx      = new Regex(SAMPLE_PATTERN);
            COM_PORT = port;
            InitializeBackgroundWorkers();
        }

        public SerialPort getPort()
        {
            return port;
        }

        /// <summary>
        /// Initialise event handlers for receiving data from Bluetooth device
        /// </summary>
        private void InitializeBackgroundWorkers()
        {
            this.portWorker.DoWork += new DoWorkEventHandler(portWorker_DoWork);
            this.portWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(portWorker_RunWorkerCompleted);
            this.portWorker.RunWorkerAsync();
        }

        /// <summary>
        /// Establishes serial port connection to Bluetooth device
        /// </summary>
        /// <param name="sender">Sender object</param>
        /// <param name="e">Event arguments</param>
        protected void portWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            if (port == null || !port.IsOpen)
            {
                // Attempt connection
                try
                {
                    Console.WriteLine("Sending command sequence to " + COM_PORT);
                    port = new SerialPort(COM_PORT, 9600, Parity.None, 8, StopBits.One); 
                    port.DataReceived += new SerialDataReceivedEventHandler(port_DataReceived);
                    port.Open();
                    port.WriteLine("device\r\n");
                    port.WriteLine("mode=2\r\n");
                    port.WriteLine("rate x 1\r\n");
                    port.WriteLine("stream=1\r\n");
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Exception: " + ex.ToString());
                }
            }
        }

        protected void port_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            byte[] temp = new byte[port.BytesToRead];
            port.Read(temp, 0, temp.Length);

            lock (data_buffer) 
            {
                data_buffer.AddRange(temp);
            }
        }

        /// <summary>
        /// Fired once serial connection is established; should signal any exceptions
        /// </summary>
        /// <param name="sender">Sender object</param>
        /// <param name="e">Event arguments</param>
        protected void portWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                MessageBox.Show("Exception: " + e.Error.ToString());
            }
        }

        /// <summary>
        /// Converts degrees to radians
        /// </summary>
        /// <param name="degrees">Angle in degrees</param>
        static float deg2rad(float degrees)
        {
            return (float)(Math.PI / 180) * degrees;
        }

        public IMUData? PollPort()
        {
            string line;

            if (!port.IsOpen) return null;

            lock (data_buffer)
            {
                int idx;
                if ((idx = data_buffer.IndexOf((int)'\n')) < 0)
                {
                    line = null;
                }
                else
                {
                    byte[] temp = data_buffer.GetRange(0, idx + 1).ToArray();
                    line = (new ASCIIEncoding()).GetString(temp);
                    data_buffer.RemoveRange(0, idx + 1);
                }
            }

            if (line == null) { return null; }

            IMUData dp = new IMUData();

            bool success = false;
            Match m = rx.Match(line);
            totalAttempts++;
            if (m.Success)
            {
                if (!initialised)
                {
                    initialised = true;
                    Console.WriteLine("IMU initialised at " + COM_PORT);
                }
                try
                {
                    float[] sample = {
                        float.Parse(m.Groups["ax"].Value) / A_DIV,
                        float.Parse(m.Groups["ay"].Value) / A_DIV,
                        float.Parse(m.Groups["az"].Value) / A_DIV,
                        deg2rad(float.Parse(m.Groups["gx"].Value) * G_MUL),
                        deg2rad(float.Parse(m.Groups["gy"].Value) * G_MUL),
                        deg2rad(float.Parse(m.Groups["gz"].Value) * G_MUL)
                    };
                    ahrs.Update(sample[3], sample[4], sample[5], sample[0], sample[1], sample[2]);
                    dp = IMUData.newPoint(sample, ahrs.Quaternion);
                    //Console.WriteLine("ax={0} ay={1} az={2} gx={3} gy={4} gx={5}", sample[0], sample[1], sample[2], sample[3], sample[4], sample[5]);
                    success = true;
                }
                catch (FormatException ex)
                {
                    Console.WriteLine(ex.StackTrace);
                }
                catch (OverflowException ex)
                {
                    Console.WriteLine(ex.StackTrace);
                }
            }
            else
            {
                droppedSamples++;
                Console.WriteLine("dropped packet. total dropped: " + droppedSamples);
            }

            if (!success) { return null; }

            return dp;
        }

        //public void clearBuffer()
        //{
        //    data_buffer.Clear();
        //}
    }
}