﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Microsoft.Surface.Samples.ScatterPuzzle
{
    public struct IMUData
    {
        public static float M_PI = 3.14159265359f;
        public string gestureID;
        public int gestureNo;
        public double unifiedTime, actualTime;
        public float[] quaternions;         // 4
        public float[] eulerAngles;         // 3
        public float[] g;                   // 3
        public float[] a;                   // 3
        public float[] b;                   // 3
        public float[] linearAcc;           // 3 
        public float[] ypr;                 // 3
        public float[] gravity;             // 3
        public float[] matrixRotation;      // 16
        public float[] matrixInvRotation;   // 16
        public float[] relAccel;            // 3

        public long startTime;
        public double relativeTime
        {
            get { return actualTime - startTime; }
        }

        public void printLinearAcc()
        {
            Console.WriteLine(this.linearAcc[0] + " " + this.linearAcc[1] + " " + this.linearAcc[2]);
        }
        public void print()
        {
            Console.WriteLine("a.x: " + this.a[0] + ", a.y: " + this.a[1] + ", a.z: " + this.a[2] + ", g.x: " + this.g[0] + ", g.y: " + this.g[1] + ", g.z: " + this.g[2]);
        }
        public static IMUData newPoint(float[] buffer, float[] quat)
        {
            long time = System.DateTime.Now.Ticks - Window1.startTime;
            IMUData dp = new IMUData();
            dp.unifiedTime = time;
            dp.actualTime = time;
            dp.a = new float[] { buffer[0], buffer[1], buffer[2] };
            dp.g = new float[] { buffer[3], buffer[4], buffer[5] };
            dp.quaternions = quat;
            
            dp.linearAcc = new float[3];     // 3
            dp.matrixRotation = new float[16];      // 16
            dp.matrixInvRotation = new float[16];   // 16
            dp.relAccel =  new float[3];            // 3

            // Euler
            dp.eulerAngles = QuaternionToEuler(quat);

            // Yaw, pitch, roll angles;  gravity relative to device
            QuaternionToYawPitchRoll(quat, out dp.ypr, out dp.gravity);

            // Calculate relative acceleration for the device after gravity is removed
            float[] point = new float[3];

            MatrixLoadZXYInverseRotation(dp.eulerAngles[2], dp.eulerAngles[1], dp.eulerAngles[0], out dp.matrixInvRotation);
            
            // (Optional) Recalculate gravity direction:
			point[0] = 0.0f; point[1] = -1.0f; point[2] = 0.0f;
            Point3MultiplyMatrix(point, dp.matrixInvRotation, out dp.gravity);

            // Re-order (YZX)
			dp.relAccel[0] = dp.a[1] + dp.gravity[0];
			dp.relAccel[1] = dp.a[2] + dp.gravity[1];
			dp.relAccel[2] = dp.a[0] + dp.gravity[2];

            // Calculate the linear acceleration in world coordinates
            point = new float[3];

			//QuaternionToMatrix(azimuth->quat, azimuth->matrixRotation);   // TODO: To use this, need to swap axes? (0=1, 1=2, 2=0)
            MatrixLoadZXYRotation(-dp.eulerAngles[2], -dp.eulerAngles[1], -dp.eulerAngles[0], out dp.matrixRotation);
            point[0] = dp.relAccel[0];
            point[1] = dp.relAccel[1];
            point[2] = dp.relAccel[2];

        	Point3MultiplyMatrix(point, dp.matrixRotation, out dp.linearAcc);

            for (int i = 0; i < dp.linearAcc.Length; i++) {
                dp.linearAcc[i] *= 9.81F;
            }

            return dp;
        }
        public static IMUData readStream(string[] str)
        {
            IMUData dp = new IMUData();
            for (int i = 0; i < str.Length; i++)
            {
                Console.WriteLine(str[i] + " |");
            }
            dp.a = readFloatArray(str, 0, 3);
            dp.g = readFloatArray(str, 3, 3);
            return dp;
        }
        public static IMUData read(String str)
        {
            IMUData dp = new IMUData();
            string[] words = str.Split(',');
            dp.gestureID = words[0];
            dp.gestureNo = Int32.Parse(words[1]);
            dp.unifiedTime = long.Parse(words[2]);
            dp.actualTime = long.Parse(words[3]);

            dp.linearAcc = readFloatArray(words, 4, 3);
            dp.a = readFloatArray(words, 7, 3);
            dp.g = readFloatArray(words, 10, 3);
            dp.b = readFloatArray(words, 13, 3);
            dp.eulerAngles = readFloatArray(words, 16, 3);
            dp.quaternions = readFloatArray(words, 19, 4);
            return dp;
        }
        private static float[] readFloatArray(String[] words, int start, int n)
        {
            float[] arr = new float[n];
            for (int i = 0; i < n; i++)
                arr[i] = float.Parse(words[start + i]);
            return arr;
        }
        #region Mathematical Functions
        private static float[] QuaternionToEuler(float[] q)
        {
            float[] angles = new float[3];
            angles[0] = (float)Math.Atan2(2 * q[1] * q[2] - 2 * q[0] * q[3], 2 * q[0] * q[0] + 2 * q[1] * q[1] - 1); // psi
            angles[1] = -(float)Math.Asin(2 * q[1] * q[3] + 2 * q[0] * q[2]); // theta
            angles[2] = (float)Math.Atan2(2 * q[2] * q[3] - 2 * q[0] * q[1], 2 * q[0] * q[0] + 2 * q[3] * q[3] - 1); // phi
            return angles;
        }

        private static void QuaternionToYawPitchRoll(float[] q, out float[] ypr, out float[] gxyz)
        {
            ypr = new float[3];
            gxyz = new float[3];

            float gx, gy, gz; // estimated gravity direction
            float sgyz, sgxz;

            gx = 2 * (q[1] * q[3] - q[0] * q[2]);
            gy = 2 * (q[0] * q[1] + q[2] * q[3]);
            gz = q[0] * q[0] - q[1] * q[1] - q[2] * q[2] + q[3] * q[3];

            sgyz = (float)Math.Sqrt(gy * gy + gz * gz);
            sgxz = (float)Math.Sqrt(gx * gx + gz * gz);

            ypr[0] = (float)Math.Atan2(2 * q[1] * q[2] - 2 * q[0] * q[3], 2 * q[0] * q[0] + 2 * q[1] * q[1] - 1);
            ypr[1] = (sgyz < 0.05f) ? 0.0f : (float)Math.Atan(gx / sgyz);
            ypr[2] = (sgxz < 0.05f) ? 0.0f : (float)Math.Atan(gy / sgxz);

            gxyz[0] = gx;
            gxyz[1] = gy;
            gxyz[2] = gz;

            // Swap axes to match output
            float t = gxyz[0];
            gxyz[0] = -gxyz[1];
            gxyz[1] = -gxyz[2];
            gxyz[2] = -t;
        }

        private static void MatrixLoadZXYRotation(float phi, float theta, float psi, out float[] matrix)
        {
	        // TODO: Speed this up by composing the matrix manually 
	        float[] rotX = new float[16], rotY = new float[16], rotZ = new float[16], temp = new float[16];
	        MatrixLoadZRotation(out rotZ, phi);    // Z: phi (roll)
	        MatrixLoadXRotation(out rotX, theta);  // X: theta (pitch)
	        MatrixLoadYRotation(out rotY, psi);    // Y: psi (yaw)
	        MatrixMultiply(rotZ, rotX, out temp);
	        MatrixMultiply(temp, rotY, out matrix);
        }

        private static void MatrixLoadZXYInverseRotation(float phi, float theta, float psi, out float[] matrix)
        {
	        // TODO: Speed this up by composing the matrix manually 
	        float[] rotX = new float[16], rotY = new float[16], rotZ = new float[16], temp = new float[16];
	        MatrixLoadZRotation(out rotZ, phi);    // Z: phi (roll)
	        MatrixLoadXRotation(out rotX, theta);  // X: theta (pitch)
	        MatrixLoadYRotation(out rotY, psi);    // Y: psi (yaw)
	        MatrixMultiply(rotY, rotX, out temp);
	        MatrixMultiply(temp, rotZ, out matrix);
        }

        private static void MatrixLoadXRotation(out float[] dest, float angleRadians)
        {
            dest = new float[16];
            dest[0] = 1.0f; dest[4] = 0.0f; dest[8] = 0.0f; dest[12] = 0.0f;
            dest[1] = 0.0f; dest[5] = (float)Math.Cos(angleRadians); dest[9] = -(float)Math.Sin(angleRadians); dest[13] = 0.0f;
            dest[2] = 0.0f; dest[6] = (float)Math.Sin(angleRadians); dest[10] = (float)Math.Cos(angleRadians); dest[14] = 0.0f;
            dest[3] = 0.0f; dest[7] = 0.0f; dest[11] = 0.0f; dest[15] = 1.0f;
        }

        private static void MatrixLoadYRotation(out float[] dest, float angleRadians)
        {
            dest = new float[16];
            dest[0] = (float)Math.Cos(angleRadians); dest[4] = 0.0f; dest[8] = (float)Math.Sin(angleRadians); dest[12] = 0.0f;
            dest[1] = 0.0f; dest[5] = 1.0f; dest[9] = 0.0f; dest[13] = 0.0f;
            dest[2] = -(float)Math.Sin(angleRadians); dest[6] = 0.0f; dest[10] = (float)Math.Cos(angleRadians); dest[14] = 0.0f;
            dest[3] = 0.0f; dest[7] = 0.0f; dest[11] = 0.0f; dest[15] = 1.0f;
        }

        private static void MatrixLoadZRotation(out float[] dest, float angleRadians)
        {
            dest = new float[16];
            dest[0] = (float)Math.Cos(angleRadians); dest[4] = -(float)Math.Sin(angleRadians); dest[8] = 0.0f; dest[12] = 0.0f;
            dest[1] = (float)Math.Sin(angleRadians); dest[5] = (float)Math.Cos(angleRadians); dest[9] = 0.0f; dest[13] = 0.0f;
            dest[2] = 0.0f; dest[6] = 0.0f; dest[10] = 1.0f; dest[14] = 0.0f;
            dest[3] = 0.0f; dest[7] = 0.0f; dest[11] = 0.0f; dest[15] = 1.0f;
        }

        private static void MatrixMultiply(float[] matrix1, float[] matrix2, out float[] result)
        {
            result = new float[16];
	        float a1, b1, c1, d1, e1, f1, g1, h1, i1, j1, k1, l1;
            float a2, b2, c2, d2, e2, f2, g2, h2, i2, j2, k2, l2;

	        // gl-arrangement
            a1 = matrix1[0];  b1 = matrix1[4];  c1 = matrix1[8];  d1 = matrix1[12];
            e1 = matrix1[1];  f1 = matrix1[5];  g1 = matrix1[9];  h1 = matrix1[13];
            i1 = matrix1[2];  j1 = matrix1[6];  k1 = matrix1[10]; l1 = matrix1[14];

            a2 = matrix2[0];  b2 = matrix2[4];  c2 = matrix2[8];  d2 = matrix2[12];
            e2 = matrix2[1];  f2 = matrix2[5];  g2 = matrix2[9];  h2 = matrix2[13];
            i2 = matrix2[2];  j2 = matrix2[6];  k2 = matrix2[10]; l2 = matrix2[14];

            result[0] = a1*a2 + b1*e2 + c1*i2;
            result[1] = e1*a2 + f1*e2 + g1*i2;
            result[2] = i1*a2 + j1*e2 + k1*i2;
            result[3] = 0.0f;

            result[4] = a1*b2 + b1*f2 + c1*j2;
            result[5] = e1*b2 + f1*f2 + g1*j2;
            result[6] = i1*b2 + j1*f2 + k1*j2;
            result[7] = 0.0f;

            result[8] = a1*c2 + b1*g2 + c1*k2;
            result[9] = e1*c2 + f1*g2 + g1*k2;
            result[10] = i1*c2 + j1*g2 + k1*k2;
            result[11] = 0.0f;

            result[12] = a1*d2 + b1*h2 + c1*l2 + d1;
            result[13] = e1*d2 + f1*h2 + g1*l2 + h1;
            result[14] = i1*d2 + j1*h2 + k1*l2 + l1;
            result[15] = 1.0f;
        }
        private static void Point3MultiplyMatrix(float[] point, float[] matrix, out float[] result)
        {
            result = new float[3];
            result[0] = matrix[0] * point[0] + matrix[4] * point[1] + matrix[8] * point[2] + matrix[12];
            result[1] = matrix[1] * point[0] + matrix[5] * point[1] + matrix[9] * point[2] + matrix[13];
            result[2] = matrix[2] * point[0] + matrix[6] * point[1] + matrix[10] * point[2] + matrix[14];
        }

        #endregion
    }

    public class IMUGesture
    {
        public int indexOfGestureInList;
        public string uniqueID; //gestureID+"-"+gestureNo;
        public string gestureID;
        public int gestureNo;
        public List<IMUData> gestureData;

        public IMUGesture()
        {
            gestureData = new List<IMUData>();
        }

        public void addIMUPoint(IMUData tp)
        {
            if (!gestureData.Exists(dp => dp.actualTime == tp.actualTime))
                gestureData.Add(tp);
        }

        public double getRelativeTime(int i)
        {
            return gestureData[i].actualTime - gestureData[30].actualTime;
        }

        public void print()
        {
            Console.WriteLine("Printing IMU gesture");
            foreach (IMUData dp in gestureData)
            {
                dp.print();
                dp.printLinearAcc();
            }
        }
    }
}
