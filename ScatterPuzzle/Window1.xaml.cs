using System;
using System.IO;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Media.Effects;
using System.Windows.Shapes;
using Microsoft.Surface.Presentation.Controls;
using SSC = Microsoft.Surface.Presentation.Controls;
/***/
using System.Collections;
using System.Collections.Generic;
using System.Windows.Input;
using SMARTTableSDK.Core;
/***/

namespace Microsoft.Surface.Samples.ScatterPuzzle
{
    /// <summary>
    /// Main Window
    /// </summary>
    public partial class Window1 : Window
    {
        #region Public Members

        public static int SENSOR_COUNT;
        public string[] comPorts;
        public static long startTime;

        public int touches = 0;  // increments with each touch down on a scatterviewitem
        public double dpiX, dpiY; // X and Y dpi of the screen

        #endregion

        #region Private Members

        // The PuzzleManager.
        private readonly PuzzleManager puzzleManager = new PuzzleManager();

        // Logging thread. Controls threads to poll data from sensors and touch screen
        private LoggingThread lgThread;

        // The puzzle image.
        private VisualBrush puzzleBrush;

        // Paths where puzzles should be loaded from.
        //private readonly string videoPuzzlesPath;
        private readonly string photoPuzzlesPath;

        // Random number generator to create random animations.
        private readonly Random random = new Random();

        // The dimensions to use when loading a puzzle.
        private int maxDifficulty = 5;
        private int minDifficulty = 2;

        // Difficulty identifiers
        private string[] difficulty_names = new string[6] { "", "", "Easy", "Medium", "Hard", "Hardest" };

        private int rowCount = 3;
        private int colCount = 3;

        // Animation events could step on each other's toes, need something to lock on
        private bool joinInProgress;

        /***/
        // to hold the list of images that will be used throughout the study
        private ArrayList images = new ArrayList();
        private ArrayList viewboxes = new ArrayList();

        private int index = 0;

        // to keep track of when a puzzle is 
        private int joins = 0;

        private Logger logger = new Logger();
        /***/

        // Brushes to use to stroke puzzle pieces
        private ArrayList brushes = new ArrayList();

        private Analyzer analyzer;

        private static PresentationSource source;

        #endregion
        #region Initalization

        //---------------------------------------------------------//
        /// <summary>
        /// Default constructor.
        /// </summary>
        public Window1()
        {
            InitializeComponent();

            analyzer = new Analyzer(this);
            lgThread = new LoggingThread(this, logger, analyzer);

            // query the registry to find out where sample photos and videos are stored.
            const string systemFoldersKey = @"HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\explorer\Shell Folders";
            photoPuzzlesPath = (string)Microsoft.Win32.Registry.GetValue(systemFoldersKey, "CommonPictures", null) + @"\Sample Pictures";

            LoadPuzzleList();
            LoadBrushes();

            Closing += Window1_Closing;

            Console.WriteLine("PixelShader 2.0 Supported: " + RenderCapability.IsPixelShaderVersionSupported(2, 0));
        }

        /// <summary>
        /// Called when the window is loaded
        /// </summary>
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
#if SUPPORT_SMART_TABLE
                // Support multi-touch on the SMART tables
                TableSDK.Instance.Register(this);
#endif

            source = PresentationSource.FromVisual(this); // must be called within window loaded

            if (source != null)
            {
                // Calculate the Slate DPI using the height and width in pixels and inches
                dpiX = (1232.6 / 10.1) * source.CompositionTarget.TransformToDevice.M11;
                dpiY = (734.7 / 5.65) * source.CompositionTarget.TransformToDevice.M22;
            }
        }

        /// <summary>
        /// Called when the window is closed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void Window1_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            logger.Save();
        }

        /// <summary>
        /// Starts the puzzle game
        /// </summary>
        private void Start()
        {
            Visual newPuzzle = (Viewbox)viewboxes[index];
            LoadVisualAsPuzzle(newPuzzle, Direction.Right);
        }
        /***/

        /// <summary>
        /// Load all the images/movies in the sample images/movies directories into the puzzle list.
        /// </summary>
        private void LoadPuzzleList()
        {
            /***/
            // Load photo puzzles
            foreach (string file in Directory.GetFiles(photoPuzzlesPath, "*.jpg"))
            {
                // Load the photo
                Image img = new Image();
                img.Source = new BitmapImage(new Uri(file));

                /***/
                // add the image to the list of images
                images.Add(img);

                Viewbox b = new Viewbox { Width = 200, Child = img };

                viewboxes.Add(b);
            }
        }

        /// <summary>
        /// Loads six brushes for use by the LoadVisualAsPuzzle function in order to stroke the edges of the puzzle pieces
        /// </summary>
        private void LoadBrushes()
        {
            // Each brush is used for a different player
            brushes.Add(Brushes.Red);
            brushes.Add(Brushes.Blue);
            brushes.Add(Brushes.Yellow);
            brushes.Add(Brushes.Green);
            brushes.Add(Brushes.Purple);
            brushes.Add(Brushes.Pink);
        }

        /// <summary>
        /// Add an image or a movie into the puzzle list
        /// </summary>
        /// <param name="img"></param>
        private void AddElementToPuzzleList(UIElement img)
        {
            // Wrap the item in a Viewbox to constrain its size.
            Viewbox b = new Viewbox { Width = 200, Child = img };

            // Insert items at random, so the videos won't all be grouped 
            // at the bottom.
            //puzzles.Items.Insert(random.Next(0, puzzles.Items.Count), b);
        }

        #endregion
        #region StartPuzzle and Change Difficulty

        //---------------------------------------------------------//
        /// <summary>
        /// Creates puzzle pieces from a visual, and adds them into the ScatterView.
        /// </summary>
        /// <param name="visual"></param>
        void LoadVisualAsPuzzle(Visual visual, Direction fromDirection)
        {
            // The more columns/rows, the less each piece needs to overlap
            float rowOverlap = PuzzleManager.Overlap / rowCount;
            float colOverlap = PuzzleManager.Overlap / colCount;

            puzzleBrush = new VisualBrush(visual);

            // Tell the puzzle manager to load a puzzle with the specified dimensions
            puzzleManager.LoadPuzzle(colCount, rowCount);

            // a colour for each piece is chosen using this index value, which is incremented to shuffle through each player in the game 
            int brush_index = 0;

            for (int row = 0; row < rowCount; row++)
            {
                for (int column = 0; column < colCount; column++)
                {
                    // Calculate the size of the rectangle that will be used to create a viewbox into the puzzle image.
                    // The size is specified as a percentage of the total image size.
                    float boxLeft = (float)column / (float)colCount;
                    float boxTop = (float)row / (float)rowCount;
                    float boxWidth = 1f / colCount;
                    float boxHeight = 1f / rowCount;

                    // Items in column 0 don't have any male puzzle parts on their side, all others do
                    if (column != 0)
                    {
                        boxLeft -= colOverlap;
                        boxWidth += colOverlap;
                    }

                    // Items in row 0 don't have any male puzzle parts on their top, all others do
                    if (row != 0)
                    {
                        boxTop -= rowOverlap;
                        boxHeight += rowOverlap;
                    }

                    // Make a visual brush based on the rectangle that was just calculated.
                    VisualBrush itemBrush = new VisualBrush(visual);
                    itemBrush.Viewbox = new Rect(boxLeft, boxTop, boxWidth, boxHeight);
                    itemBrush.ViewboxUnits = BrushMappingMode.RelativeToBoundingBox;

                    // Get the shape of the piece
                    Geometry shape = GetPieceGeometry(column, row);

                    /***/
                    double scale = 1.0; // 0.5 + random.NextDouble() * (1.0 - 0.5);
                    /***/

                    //shape = new RectangleGeometry(new Rect(5, 5, 200, 200));

                    // Put the brush into a puzzle piece
                    PuzzlePiece piece = new PuzzlePiece(column + (colCount * row), shape, itemBrush);

                    // Set the sensor ID of this piece for identification purposes
                    piece.sensorID = brush_index;

                    // Add a stroke to the puzzle piece to identify it visually
                    // Use this if not using a separate edge object within the canvas
                    //piece.Stroke = (Brush)brushes[brush_index];
                    //piece.StrokeThickness = 5.5;

                    // increment the brush index so that we use a different colour for the next piece
                    brush_index++; // make sure we reset the index before we reach a colour for a non-existant player
                    if (brush_index >= SENSOR_COUNT) brush_index = 0;

                    /***/
                    piece.setScale(scale);
                    /***/

                    // Setup label to take touch count text
                    Label label = new Label();
                    label.Content = "0";
                    label.FontSize = 72.0;
                    label.Foreground = Brushes.Yellow;
                    label.Padding = new Thickness(2.0);
                    label.Visibility = Visibility.Hidden;
                    DropShadowEffect shadow = new DropShadowEffect();
                    shadow.ShadowDepth = 0;
                    label.Effect = shadow;

                    Canvas.SetRight(label, 40); // Alignment- this should be roughly fine
                    Canvas.SetTop(label, 20);

                    //Grid grid = new Grid();
                    //grid.Width = piece.Width;
                    //grid.Height = piece.Height;

                    // Create stroked edge separately
                    System.Windows.Shapes.Path edge = new System.Windows.Shapes.Path();
                    edge.Data = shape;
                    edge.Stroke = (Brush)brushes[brush_index];
                    edge.StrokeThickness = 5.0;

                    // Add canvas to group them all together
                    Canvas group = new Canvas();
                    group.Children.Add(piece);
                    group.Children.Add(edge);
                    group.Children.Add(label);

                    // Add the PuzzlePiece to a ScatterViewItem
                    ScatterViewItem item = new ScatterViewItem();
                    item.Content = group;

                    // setup logging
                    item.Uid = ((row * colCount) + column).ToString();

                    // Handle touch events
                    item.PreviewTouchDown += item_TouchDown;
                    item.PreviewTouchUp += item_TouchUp;

                    // Register a name for this item with this class for animation purposes
                    string piecename = "ScatterViewItem" + (column + (colCount * row));

                    piece.animationName = piecename;

                    if (this.FindName(piecename) != null)
                    {
                        this.UnregisterName(piecename);
                    }
                    this.RegisterName(piece.animationName, item);

                    /***/
                    item.Width = Math.Round(piece.ClipShape.Bounds.Width * scale, 0);
                    item.Height = Math.Round(piece.ClipShape.Bounds.Height * scale, 0);

                    //item.SizeChanged += new SizeChangedEventHandler(item_SizeChanged);

                    /***/
                    // Set the initial size of the item and prevent it from being resized
                    //item.Width = Math.Round(piece.ClipShape.Bounds.Width, 0);
                    //item.Height = Math.Round(piece.ClipShape.Bounds.Height, 0);
                    // we want to be able to scale the item
                    item.CanScale = false;

                    // Set the item's data context so it can use the piece's shape
                    Binding binding = new Binding();
                    binding.Source = group;
                    item.SetBinding(ScatterViewItem.DataContextProperty, binding);

                    // Animate the item into view
                    AddPiece(item, fromDirection);
                }
            }
        }

        //---------------------------------------------------------//
        /// <summary>
        /// Returns a geometry that represents the shape of the piece, determined by the piece's row and column.
        /// </summary>
        /// <param name="row">The piece's row</param>
        /// <param name="column">The piece's column</param>
        private Geometry GetPieceGeometry(int column, int row)
        {
            System.Windows.Shapes.Path path;
            if (row == 0)
            {
                if (column == 0)
                {
                    path = (System.Windows.Shapes.Path)Resources["TopLeftCorner"];
                }
                else if (column == colCount - 1)
                {
                    path = (System.Windows.Shapes.Path)Resources["TopRightCorner"];
                }
                else
                {
                    path = (System.Windows.Shapes.Path)Resources["TopEdge"];
                }
            }
            else if (row == rowCount - 1)
            {
                if (column == 0)
                {
                    path = (System.Windows.Shapes.Path)Resources["BottomLeftCorner"];
                }
                else if (column == colCount - 1)
                {
                    path = (System.Windows.Shapes.Path)Resources["BottomRightCorner"];
                }
                else
                {
                    path = (System.Windows.Shapes.Path)Resources["BottomEdge"];
                }
            }
            else if (column == 0)
            {
                path = (System.Windows.Shapes.Path)Resources["LeftEdge"];
            }
            else if (column == colCount - 1)
            {
                path = (System.Windows.Shapes.Path)Resources["RightEdge"];
            }
            else
            {
                path = (System.Windows.Shapes.Path)Resources["Center"];
            }

            return path.Data.GetFlattenedPathGeometry();
        }

        //---------------------------------------------------------//
        /// <summary>
        /// returns the application to its initial state where all movies are 
        /// playing and muted, and no pieces are on the board. 
        /// </summary>
        void Reset()
        {
            // If a puzzle is currently loaded and it is a video, mute it
            if (puzzleBrush != null)
            {
                MediaElement media = puzzleBrush.Visual as MediaElement;
                if (media != null)
                {
                    media.IsMuted = true;
                }
            }

            // Remove all puzzle pieces
            RemoveAllPieces();
        }

        #endregion
        #region ScatterViewItem Events

        /// <summary>
        /// Called when a touch occurs on a ScatterViewItem
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void item_TouchDown(object sender, TouchEventArgs e)
        {
            System.Windows.Input.TouchPoint touch = e.GetTouchPoint(this);
            handleItemGrab(touch.Position.X, touch.Position.Y, sender as ScatterViewItem, e.TouchDevice);
        }

        /// <summary>
        /// Called when the ScatterViewItem stops being touched
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void item_TouchUp(object sender, TouchEventArgs e)
        {
            handleItemRelease(sender as ScatterViewItem, e.TouchDevice);
        }

        /// <summary>
        /// Handle a touch down event for touch or mouse in an input-agnostic way, 
        /// to reduce code-reuse between input methods
        /// </summary>
        /// <param name="touchX"></param>
        /// <param name="touchY"></param>
        /// <param name="item"></param>
        void handleItemGrab(double touchX, double touchY, ScatterViewItem item, TouchDevice inputDevice)
        {
            // The puzzle piece class is stored in the 'Content' property of the ScatterViewItem
            PuzzlePiece pp = PuzzleManager.getPuzzlePieceFromScatterViewItem(item);

            // Increment touch counter for label
            touches++;
            totaltoucheslabel.Content = touches;
            Label label = getLabelFromCanvas((Canvas)item.Content);
            label.Content = touches;
            label.Visibility = Visibility.Visible;


            // apply greyscale effect
            if (!pp.beingTouched())
            {
                pp.Effect = new Effects.GreyscaleEffect();

                /** Add circle on touch location **/
                /* var circle = new TouchCircle(pp);

                 canvas.Children.Add(circle);

                 Canvas.SetLeft(circle, touchX - circle.Width / 2);
                 Canvas.SetTop(circle, touchY - circle.Height / 2);
                 */
            }

            pp.touchesDown++;
            Console.WriteLine("touches: " + pp.touchesDown);

            pp.lastPosition = item.Center;

            // Call the logging thread to handle polling the touch device
            lgThread.touchDown(inputDevice, item);
        }

        /// <summary>
        /// Handle releasing a ScatterViewItem in an input-agnostic way
        /// </summary>
        /// <param name="item"></param>
        void handleItemRelease(ScatterViewItem item, TouchDevice inputDevice)
        {
            PuzzlePiece pp = PuzzleManager.getPuzzlePieceFromScatterViewItem(item);

            Label label = getLabelFromCanvas((Canvas)item.Content);
            label.Visibility = Visibility.Hidden;

            lgThread.touchUp(inputDevice);

            if (pp.beingTouched())
            {
                pp.touchesDown--;

                if (!pp.beingTouched())
                {
                    pp.Effect = null;
                }

                // Validate input
                if (item == null)
                    return;

                // If there are any pieces the can be joined, then join them
                StartJoinIfPossible(item);
            }
        }

        /// <summary>
        /// Called when an item is scaled by the user
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void item_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            ScatterViewItem viewItem = (ScatterViewItem)sender;
            Canvas content = (Canvas)viewItem.Content;

            PuzzlePiece piece = PuzzleManager.getPuzzlePieceFromScatterViewItem(viewItem);

            double scale = viewItem.Width / piece.originalWidth;

            piece.ClipShape.Transform = new ScaleTransform(scale, scale);
            piece.setScale(scale);
            piece.Height = Math.Round(piece.ClipShape.Bounds.Height, 0);
            //piece.originalHeight = piece.Height;
            piece.Width = Math.Round(piece.ClipShape.Bounds.Width, 0);
        }

        /***/
        #endregion
        #region Identification

        /// <summary>
        /// Makes an online identification for each touch signal using the overall calculated difference
        /// between the touch signal and each IMU signal
        /// </summary>
        /// <param name="touchKeys">The touch devices which are used as the key in the Analyzer's touches dictionary</param>
        /// <param name="corrD">The difference values for each IMU signal in respect to each touch signal</param>
        /// <param name="signalLengths">The length of the signals in ms</param>
        public void onlineIdentification(TouchDevice[] touchKeys, double[][] corrD, int[] signalLengths)
        {
            /*double threshold = 30;
            // i specifies the touch
            for (int i = 0; i < corrD.Length; i++)
            {
                double minCorr = double.MaxValue;
                int identifiedSensorID = 0;

                double[] diff = new double[corrD[i].Length - 1];

                // j specifies the sensor
                for (int j = 0; j < corrD[i].Length; j++)
                {
                    if (corrD[i][j] < minCorr)
                    {
                        minCorr = corrD[i][j];
                        identifiedSensorID = j;
                    }
                }

                // now find the differences
                for (int k = 0, j = 0; k < diff.Length; k++, j++)
                {
                    if (corrD[i][j] >= corrD[i][j + 1])
                        diff[k] = corrD[i][j] - corrD[i][j + 1];
                    else if (corrD[i][j] < corrD[i][j + 1])
                        diff[k] = corrD[i][j + 1] - corrD[i][j];
                    Console.WriteLine("diff[" + k + "] = " + diff[k]);
                }
                if (allDifferencesAboveThreshold(diff) && signalLengths[i] > 30)
                {
                    Console.WriteLine("identified touch as sensor " + identifiedSensorID);
                    parentWindow.identifySensor(touchDownDevice, identifiedSensorID, corD, signalLength);
                }
            }*/
        }

        /// <summary>
        /// If the calculated differences for each touch and IMU signal pair are all above a certain
        /// threshold then we can make the decision that the IMU signal with the lowest difference
        /// must be the signal associated with the target touch signal
        /// </summary>
        /// <param name="diff">The calculated differences between the total mangitude difference
        /// values</param>
        /// <returns>Are the calculated differences above the threshold or not</returns>
        private bool allDifferencesAboveThreshold(double[] diff)
        {
            double threshold = 50;
            for (int i = 0; i < diff.Length; i++)
            {
                if (diff[i] < threshold)
                    return false;
            }
            return true;
        }

        /// <summary>
        /// Used to identify a touch signal with an IMU signal and acts accordingly
        /// </summary>
        /// <param name="touchDevice"></param>
        /// <param name="identifiedSensorID"></param>
        /// <param name="diff"></param>
        /// <param name="length"></param>
        public void identifySensor(TouchDevice touchDevice, int identifiedSensorID, double[] diff, int length)
        {
            ScatterViewItem item = analyzer.puzzlePieces[touchDevice];
            PuzzlePiece piece = PuzzleManager.getPuzzlePieceFromScatterViewItem(item);
            Console.WriteLine("pieceID: " + piece.sensorID + ", identifiedSensorID: " + identifiedSensorID);

            // If the piece associated with the touch signal is not associated with the IMU that was
            // identified then we want to move the piece back to it's original position
            if (piece.sensorID != identifiedSensorID)
            {
                MovePiece(item, piece.lastPosition);
            }
            logger.logIdentification(touches, 0, 0, new double[] { 0.0 }, identifiedSensorID);
        }

        #endregion
        #region Puzzle Assembly

        //---------------------------------------------------------//
        /// <summary>
        /// Occurs when a manipulationCompleted event is fired.
        /// </summary>
        /// <remarks>
        /// An item has been moved. See if it is placed near another item that to which it can be joined.
        /// </remarks>
        /// <param name="sender">The object that raized the event.</param>
        /// <param name="args">The arguments for the event.</param>
        private void scatter_ScatterManipulationCompleted(object sender, ManipulationCompletedEventArgs e)
        {
            // Not sure if this method is still needed
            logger.LogManipulation();
            //Console.WriteLine("called scatter_ScatterManipulationCompleted");

            // Validate input
            ScatterViewItem item = e.OriginalSource as ScatterViewItem;
            if (item == null)
            {
                return;
            }

            // If there are any pieces the can be joined, then join them
            StartJoinIfPossible(item);
        }

        //---------------------------------------------------------//
        /// <summary>
        /// Search the puzzle area for a ScatterViewItem that can be joined to the piece that was 
        /// passed as an argument. If a match is found, join the pieces.
        /// </summary>
        /// <param name="item">The piece that could potentially be joined to another piece.</param>
        private void StartJoinIfPossible(ScatterViewItem item)
        {
            // Compare this piece against all the other pieces in the puzzle
            foreach (ScatterViewItem potentialItem in scatter.Items)
            {
                // Don't even bother trying to join a piece to itself
                if (potentialItem == item)
                {
                    continue;
                }

                // See if the pieces are eligible to join
                if (puzzleManager.CanItemsJoin(item, potentialItem))
                {
                    // log join
                    logger.LogJoin();

                    // The pieces are eligible, join them
                    JoinItems(potentialItem, item);

                    // Only join one set of pieces per manipulation
                    break;
                }
            }
        }

        #endregion
        #region Animations

        //---------------------------------------------------------//
        /// <summary>
        /// Animates all current pieces off the side of the screen.
        /// </summary>
        private void RemoveAllPieces()
        {
            if (scatter.Items.Count == 0)
            {
                //selectionEnabled = true;
                return;
            }

            // Use a for loop here instead of a foreach so the variable used in the animation complete 
            // callback is not modified between the time the callback is hooked up and the time it is called.
            for (int i = 0; i < scatter.Items.Count; i++)
            {
                ScatterViewItem item = (ScatterViewItem)scatter.Items[i];
                PointAnimation remove = ((PointAnimation)Resources["RemovePiece"]).Clone();

                // Can't animate if center isn't set yet, which would happen if a piece has not yet been manipulated
                if (double.IsNaN(item.Center.X))
                {
                    item.Center = item.ActualCenter;
                }

                // Set up a callback that passes the ScatterViewItem that will be needed when the animation completes
                remove.Completed += delegate(object sender, EventArgs e)
                {
                    OnRemoveAnimationCompleted(item);
                };

                // Start the animation
                item.BeginAnimation(ScatterViewItem.CenterProperty, remove, HandoffBehavior.SnapshotAndReplace);
            }
        }

        //---------------------------------------------------------//
        /// <summary>
        /// Called when a remove animation has been completed.
        /// </summary>
        /// <param name="item">The item that was animated</param>
        private void OnRemoveAnimationCompleted(ScatterViewItem item)
        {
            scatter.Items.Remove(item);
            //selectionEnabled = true;
        }

        //---------------------------------------------------------//
        /// <summary>
        /// Adds a ScatterViewItem to the ScatterView, and animates it 
        /// on from the specified side of the screen (Left or Right).
        /// </summary>
        /// <param name="item">The item to add.</param>
        /// <param name="fromDirection">The direction from which puzzle pieces enter.</param>
        private void AddPiece(ScatterViewItem item, Direction fromDirection)
        {
            // Add the piece to the ScatterView at the correct location
            Debug.Assert(fromDirection == Direction.Right || fromDirection == Direction.Left);
            item.Center = fromDirection == Direction.Right ? new Point(1024, 384) : new Point(-200, 384);
            item.Orientation = random.Next(0, 360);
            scatter.Items.Add(item);

            // Load the animation
            Storyboard add = ((Storyboard)Resources["AddPiece"]).Clone();

            foreach (AnimationTimeline animation in add.Children)
            {
                // If this is a double animation, it animates the item's orientation
                DoubleAnimation orientation = animation as DoubleAnimation;
                if (orientation != null)
                {
                    // Spin the orientation a little.
                    orientation.To = item.Orientation + random.Next(-135, 135);
                }

                // If this is a point animation, then it animates the item's center
                PointAnimation center = animation as PointAnimation;
                if (center != null)
                {
                    // Get a random point to animate the item to
                    center.To = new Point(random.Next(0, 773), random.Next(0, 768));
                }
            }

            // Set up a callback that passes the ScatterViewItem that will be needed when the animation completes
            add.Completed += delegate(object sender, EventArgs e)
            {
                OnAddAnimationCompleted(item);
            };

            // Start the animation
            item.BeginStoryboard(add, HandoffBehavior.SnapshotAndReplace);
        }

        /// <summary>
        /// Animates a ScatterViewItem to the designated point
        /// </summary>
        /// <param name="item">The ScatterViewItem to animate</param>
        /// <param name="target">The point in the window to move the ScatterViewItem to</param>
        private void MovePiece(ScatterViewItem item, Point target)
        {
            PointAnimation targetCenter = new PointAnimation();
            targetCenter.Duration = TimeSpan.FromSeconds(0.2);
            targetCenter.From = item.Center;
            targetCenter.To = target;
            targetCenter.FillBehavior = FillBehavior.Stop;

            Storyboard.SetTargetName(targetCenter, PuzzleManager.getPuzzlePieceFromScatterViewItem(item).animationName);
            Storyboard.SetTargetProperty(
                targetCenter, new PropertyPath(ScatterViewItem.CenterProperty));

            Storyboard move = new Storyboard();
            move.Children.Add(targetCenter);

            // When the animation is completed then we want to move the item to it's target and cancel the
            // animation so that the piece can move again
            move.Completed += delegate(object sender, EventArgs e)
            {
                item.Center = target;
                item.CancelManipulation();
            };

            move.Begin(this);
        }

        //---------------------------------------------------------//
        /// <summary>
        /// Called when an add animation is completed.
        /// </summary>
        /// <param name="item">The item that was just animated.</param>
        private static void OnAddAnimationCompleted(ScatterViewItem item)
        {
            // When the animation completes, the animation will no longer be the determining factor for the layout
            // of the item, it will revert to the value with the nextmost precedence. In this case, it will be the 
            // values assigned to the item's center and orientation. Set those values to their current values (while
            // the item is still under animation) so that when the animation completes, the item won't appear to jump.
            item.Center = item.ActualCenter;
            item.Orientation = item.ActualOrientation;
        }

        //---------------------------------------------------------//
        /// <summary>
        /// Animates two ScatterViewItems together and then merges the content of both items into one ScatterViewItem.
        /// </summary>
        /// <param name="pieceRemaining">The piece that will remain after the join.</param>
        /// <param name="pieceBeingRemoved">The piece that will be removed as a result of the join.</param>
        private void JoinItems(ScatterViewItem pieceRemaining, ScatterViewItem pieceBeingRemoved)
        {
            // Simultaneous joins on the same pieces (as in the case where two matching pieces are dropped next 
            // to each other at the same time) eventually remove both matching pieces. Make sure only one join
            // happens at one time.
            if (!joinInProgress)
            {
                joinInProgress = true;

                Storyboard join = ((Storyboard)Resources["JoinPiece"]).Clone();

                foreach (AnimationTimeline animation in join.Children)
                {
                    // If this is a double animation, then it animates the piece's orientation
                    DoubleAnimation orientation = animation as DoubleAnimation;
                    if (orientation != null)
                    {
                        orientation.To = pieceRemaining.ActualOrientation;
                        orientation.From = pieceBeingRemoved.ActualOrientation;

                        // If two pieces are close in orientation, but seperated by the 0/360 line (i.e. 3 and 357) then don't spin the piece all the way around
                        if (Math.Abs(pieceRemaining.ActualOrientation - pieceBeingRemoved.ActualOrientation) > 180)
                        {
                            orientation.To += orientation.From > orientation.To ? 360 : -360;
                        }
                    }

                    // If this is a point animation, then it animates the piece's center
                    PointAnimation center = animation as PointAnimation;
                    if (center != null)
                    {
                        center.To = puzzleManager.CalculateJoinAnimationDestination(pieceRemaining, pieceBeingRemoved);
                    }

                    // Can't animate values that are set to NaN
                    if (double.IsNaN(pieceBeingRemoved.Orientation))
                    {
                        pieceBeingRemoved.Orientation = pieceBeingRemoved.ActualOrientation;
                    }

                    // Set up a callback that passes the ScatterViewItems that will be needed when the animation completes
                    join.Completed += delegate(object sender, EventArgs e)
                    {
                        OnJoinAnimationCompleted(pieceBeingRemoved, pieceRemaining);
                    };

                    pieceBeingRemoved.BeginStoryboard(join);
                }
            }
        }

        //---------------------------------------------------------//
        /// <summary>
        /// Called when an join animation is completed.
        /// </summary>
        /// <param name="itemBeingRemoved">The item that should be removed after the join.</param>
        /// <param name="itemRemaining">The item that will remain after the join.</param>
        private void OnJoinAnimationCompleted(ScatterViewItem itemBeingRemoved, ScatterViewItem itemRemaining)
        {
            if (scatter.Items.Contains(itemBeingRemoved) && scatter.Items.Contains(itemRemaining))
            {
                // Get the content for the joined piece
                PuzzlePiece pieceBeingRemoved = PuzzleManager.getPuzzlePieceFromScatterViewItem(itemBeingRemoved);
                PuzzlePiece pieceRemaining = PuzzleManager.getPuzzlePieceFromScatterViewItem(itemRemaining);

                PuzzlePiece joinedPiece = puzzleManager.JoinPieces(pieceBeingRemoved, pieceRemaining);

                // When size changes, center also must be adjusted so the piece doesn't jump
                Vector centerAdjustment = puzzleManager.CalculateJoinCenterAdjustment(pieceRemaining, joinedPiece);
                centerAdjustment = PuzzleManager.Rotate(centerAdjustment, itemRemaining.ActualOrientation);

                // Replace the item's content with the new group
                Canvas canvas = (Canvas)itemRemaining.Content;

                // Get the label out of the canvas to re-add
                Label label = getLabelFromCanvas(canvas);

                // Get label and re-add it to the canvas after clearing
                canvas.Children.Clear();
                canvas.Children.Add(joinedPiece);
                canvas.Children.Add(label);

                itemRemaining.Content = canvas;


                /***/
                double scale = pieceBeingRemoved.getScale();
                joinedPiece.setScale(scale);

                // it seems the problem presists with the bare minimum, changing the width of the item is intact but not it's image
                /***/
                // Resize the item to the size of the peice
                itemRemaining.Width = Math.Round(joinedPiece.ClipShape.Bounds.Width * scale, 0);
                itemRemaining.Height = Math.Round(joinedPiece.ClipShape.Bounds.Height * scale, 0);

                // Adjust the center
                itemRemaining.Center = itemRemaining.ActualCenter + centerAdjustment;

                // Bind the item to the new piece
                Binding binding = new Binding();
                binding.Source = joinedPiece;
                itemRemaining.SetBinding(ScatterViewItem.DataContextProperty, binding);
                itemRemaining.SetRelativeZIndex(RelativeScatterViewZIndex.AboveInactiveItems);

                // Remove the old item from the ScatterView
                scatter.Items.Remove(itemBeingRemoved);

                /***/
                joins = joins + 1;
                /***/

                // Set this to false before StartJoinIfPossible. If there is another join, JoinPieces will set joinInProgress to true,
                // and then it will immediately be set back when StartJoinIfPossible returns.
                joinInProgress = false;

                // See if there are any other pieces elligible to join to the newly joined piece
                StartJoinIfPossible(itemRemaining);
            }
            else
            {
                // Still want to set this to false, even if no join was performed
                joinInProgress = false;
            }

            /***/
            if (puzzleCompleted())
            {
                // log completed
                logger.LogCompleted();

                // reset to remove the completed puzzle
                Reset();
                Visual newPuzzle = (Viewbox)viewboxes[index];
                LoadVisualAsPuzzle(newPuzzle, Direction.Left);
            }
            /***/
        }

        /// <summary>
        /// Utility method for getting a label out of its canvas
        /// </summary>
        /// <param name="canvas"></param>
        /// <returns></returns>
        private Label getLabelFromCanvas(Canvas canvas)
        {
            foreach (UIElement child in canvas.Children)
            {
                if (child is Label)
                {
                    return (Label)child;
                }
            }
            return null;
        }

        /// <summary>
        /// Called when a join is completed to check whether the puzzle is completed
        /// </summary>
        /// <returns>Has the puzzle completed or not</returns>
        private bool puzzleCompleted()
        {
            // The number of joins to complete the puzzle is
            // the number of pieces minus one.
            if (joins == rowCount * colCount - 1)
            {
                if (index == viewboxes.Count - 1)
                {
                    index = 0;
                }
                else
                {
                    index = index + 1;
                }
                joins = 0;

                return true;
            }
            return false;
        }

        private void ShowImage()
        {
            scatter.Visibility = Visibility.Hidden;
            puzzleImage.Visibility = Visibility.Visible;
            currentImage.Source = ((Image)images[index]).Source;
        }

        #endregion
        #region Button Callbacks

        /// <summary>
        /// Called when the user clicks the connect button in order to connect to the IMU's over
        /// bluetooth and start the puzzle game
        /// </summary>
        private void connectBtn_Click(object sender, RoutedEventArgs e)
        {
            if (comPorts != null)
            {
                menuPanel.Visibility = Visibility.Hidden;
                menuPanel.IsEnabled = false;
                SENSOR_COUNT = comPorts.Length;

                for (int i = 0; i < comPorts.Length; i++)
                {
                    comPorts[i].ToUpper();
                    lgThread.cwas.Add(new CWASensor(comPorts[i]));
                }

                startTime = DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond;
                lgThread.StartLogging();
                analyzer.initialiseOnline(SENSOR_COUNT, 5);
                Start();
            }
        }

        /// <summary>
        /// Shows the settings option box so that the user can select which COM ports to connect to
        /// the sensors over
        /// </summary>
        private void settingsBtn_Click(object sender, RoutedEventArgs e)
        {
            Settings stsWindow = new Settings(this);
            stsWindow.ShowDialog();
        }

        /// <summary>
        /// Allows the user to switch between different difficulty modes aka. the number of pieces
        /// on the game board
        /// </summary>
        private void difficultyBtn_Click(object sender, RoutedEventArgs e)
        {
            rowCount++;
            colCount++;

            if (rowCount > maxDifficulty)
                rowCount = colCount = minDifficulty;

            difficultyBtn.Content = difficulty_names[rowCount];
        }
        
        #endregion
    }
}