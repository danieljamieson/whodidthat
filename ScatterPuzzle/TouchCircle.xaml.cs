﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Media.Animation;

namespace Microsoft.Surface.Samples.ScatterPuzzle
{
    /// <summary>
    /// Interaction logic for TouchCircle.xaml
    /// </summary>
    public partial class TouchCircle : UserControl
    {
        // Animation storyboard
        private Storyboard b;

        // Puzzle piece this start animation is associated with
        private PuzzlePiece pp;

        public TouchCircle(PuzzlePiece piece)
        {
            InitializeComponent();

            b = (Storyboard) this.FindResource("animate");
            b.Completed += new EventHandler(this.animationCompleted);

            this.pp = piece;
        }

        public Storyboard getStoryboard()
        {
            return b;
        }

        // Example of adding event handler to Storyboard.Completed
        private void animationCompleted(object sender, EventArgs e)
        {
            pp.IsManipulationEnabled = true;

            (this.Parent as Canvas).Children.Remove(this);
            //(App.Current.MainWindow as Window1).canvas.Children.Remove(this);
       }
    }
}
