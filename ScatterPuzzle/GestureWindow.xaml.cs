﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Microsoft.Surface.Samples.ScatterPuzzle
{
    /// <summary>
    /// Interaction logic for GestureWindow.xaml
    /// </summary>
    public partial class GestureWindow : Window
    {
        #region Initialisation

        private string title = "";
        private SolidColorBrush brush;

        // constructor for IMU gestures
        public GestureWindow(string _title)
        {
            InitializeComponent();
            title = _title;
            gestureTitleLabel.Content = title;
            brush = new SolidColorBrush(Colors.Red);
        }

        #endregion

        #region Draw IMU Gestures

        public void drawTTGraphs(double[] accX, double[] accY, double[] accD)
        {
            gestureCanvas.Children.Clear();
            drawGraphs(accX, accY, accD, gestureCanvas,/*ttScaleSlider.Value,*/Brushes.Blue);
        }

        public void drawIMUGraphs(double[] accX, double[] accY, double[] accD, double[] times, int startX, int startY)
        {
            //IMUGraphsCanva.Children.Clear();
            drawGraphs(accX, accY, accD, gestureCanvas, /*IMUScaleSlider.Value,*/Brushes.Red);
            //drawXYfromAcc(accX, accY, times, tabletopGesturesCanva, startX, startY);
            ScaleTransform st = new ScaleTransform(0.3, 0.3);
            //imuPath.LayoutTransform = st;
        }

        private void drawGraphs(double[] accX, double[] accY, double[] accD, Canvas drCanvas, Brush brsh)
        {
            Polyline accXPath, accYPath, accDPath;
            accXPath = createPath(brsh, 2, drCanvas);
            accYPath = createPath(brsh, 2, drCanvas);
            accDPath = createPath(brsh, 2, drCanvas);
            int YAxesDelta = (int)drCanvas.RenderSize.Height / 4;
            Canvas.SetTop(accXPath, YAxesDelta);
            drawGraph(accXPath, accX, drCanvas);

            Canvas.SetTop(accYPath, YAxesDelta * 3);
            drawGraph(accYPath, accY, drCanvas);
        }
        private void drawGraph(Polyline pl, double[] y, Canvas drCanvas)
        {
            if (y == null)
                return;
            int totalX = (int)drCanvas.RenderSize.Width;
            int xInterval = totalX / y.Length;
            Point pt;
            double min = double.MaxValue;
            double max = double.MinValue;
            for (int x = 0, i = 0; i < y.Length; i++, x += xInterval)
            {
                if (y[i] > max)
                    max = y[i];
                else if (y[i] < min)
                    min = y[i];
            }
            double yScale = 150 / (max - min);
            for (int x = 0, i = 0; i < y.Length; i++, x += xInterval)
            {
                pt = new Point(x, y[i] * yScale);
                //pt = new Point(x, y[i]) ;
                pl.Points.Add(pt);
            }
        }

        #endregion

        #region Drawing Touch Gestures

        public void DrawTouchGesture(TouchGesture tg)
        {
            gestureCanvas.Children.Clear();
            drawTouchGesture(tg, brush, gestureCanvas);
        }

        private void drawTouchGesture(TouchGesture tg, Brush brsh, Canvas canvas)
        {
            Polyline pth = createPath(brsh, 20, canvas);
            Ellipse elps = new Ellipse();
            canvas.Children.Add(elps);
            elps.Stroke = brsh;
            elps.StrokeThickness = 20;
            elps.Stroke = brsh;
            PointCollection myPointCollection;
            myPointCollection = new PointCollection();
            pth.Points.Clear();
            Point pt = new Point(0, 0);
            bool bFirst = true;
            double scale = 0.3;
            foreach (TouchPoint tp in tg.gestureData)
            {
                pt = new Point(tp.x, tp.y);
                if (bFirst)
                {
                    myPointCollection.Add(pt);
                    bFirst = false;
                    elps.Width = 50;
                    elps.Height = 50;
                    Canvas.SetLeft(elps, (pt.X - 25) * scale);
                    Canvas.SetTop(elps, (pt.Y - 25) * scale);
                }
                myPointCollection.Add(pt);
            }
            if (tg.gestureData.Count() == 1 && pt != null)
                myPointCollection.Add(pt);
            pth.Points = myPointCollection;
            ScaleTransform st = new ScaleTransform(scale, scale);
            pth.LayoutTransform = st;
            elps.LayoutTransform = st;
        }

        private Polyline createPath(Brush brsh, int thickness, Canvas parentCanvas)
        {
            Polyline pl = new Polyline();
            parentCanvas.Children.Add(pl);
            pl.Stretch = Stretch.None;
            pl.StrokeEndLineCap = PenLineCap.Round;
            pl.StrokeLineJoin = PenLineJoin.Round;
            Canvas.SetLeft(pl, 0);
            Canvas.SetTop(pl, 0);
            pl.Stroke = brsh;
            pl.StrokeThickness = thickness;
            return pl;
        }
        #endregion
    }
}
